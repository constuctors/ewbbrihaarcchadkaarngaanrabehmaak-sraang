<?php 
	ob_start();
	session_start();
?>
<?php 
	require './connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผลงานต่างๆ</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		
		<script>

		$(document).ready(function(){
			
			$("#showeall").click(function(){
				$("#cat1").slideDown(1000);
				$("#cat2").slideDown(1000);
			});
			$("#show1").click(function(){
				$("#cat1").slideDown(1000);
				$("#cat2").slideUp(1000);
			 });
			 $("#show2").click(function(){
				$("#cat2").slideDown(1000);
				$("#cat1").slideUp(1000);
			 });		
		});
		</script>
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div id="site-content">
			
			<header class="site-header">
				<div class="top-header">
					<div class="container">
						<a href="tel:80049123441">Call Us: 086-478-1761</a>
						
						<nav class="member-navigation pull-right">
							<?php  if($_SESSION['use_id'] != "")
							{
							?>
								<a href="detailuser.php"><i class="fa fa-user"></i>
									<?php echo $objresult['use_fname'];echo "&nbsp";echo $objresult['use_lname'];?>
								</a>
								<a href="logout.php"><i class="fa fa-lock"></i> ออกจากระบบ</a>
							<?php }else{ ?>
							<a href="fromLogin.php"><i class="fa fa-user"></i> Register</a>	
							<a href="fromLogin.php"><i class="fa fa-lock"></i> Login</a>
							<?php } ?>
						</nav> <!-- .member-navigation -->
					</div> <!-- .container -->
				</div> <!-- .top-header -->

				<div class="bottom-header">
					<div class="container">
						<a href="index.php" class="branding pull-left">
							<img src="images/logo-icon.png" alt="Site title" class="logo-icon">
							<h1 class="site-title">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></h1> 
							<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
						</a> <!-- #branding -->
						
						<nav class="main-navigation pull-right">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item"><a href="profilenews.php">ผลงาน</a></li>
								<li class="menu-item"><a href="showjob.php">ตรวจสอบราคางาน</a></li>
								<li class="menu-item"><a href="contact.php">ติดต่องาน</a></li>
							</ul>
						</nav> <!-- .main-navigation -->
					</div> <!-- .container -->
				</div> <!-- .bottom-header -->
			</header> <!-- .site-header -->

			<main class="content">
				<div class="breadcrumbs">
					<div class="container">
						<a href="index.php">หน้าแรก</a> &rarr;
						<a href="profilenews.php">ผลงาน</a>
					</div>
				</div>

				<div class="inner-content">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
			<?php 
				 $selpro =	"	select * 
							from profile , category
							where profile.pro_type = category.cate_id
						" ;
				$qrpro = mysqli_query($condb,$selpro);
			 while ($arrpro = mysqli_fetch_array($qrpro)) 
			{
			?>
				<article class="post hentry wow fadeInUp"  id="cat<?php echo $arrpro['pro_type'];?>">
					<h2 class="entry-title">
						<?php echo $arrpro['pro_name']; ?>
					</h2>
					<figure class="featured-image">
					<?php
						$selimg =	"	select *
									from propicture as pp
									where pp.pro_id = ' ".$arrpro['pro_id']." '
								";
						$qrimg = mysqli_query($condb,$selimg);
						$objimg = mysqli_fetch_array($qrimg,MYSQLI_ASSOC);
					 ?>
						<img src="picture/profile/<?php echo $objimg['ppic_name']; ?>" height = "400" alt="">
					</figure>
					<div class="entry-summary">
						<p><?php echo $arrpro['pro_description']; ?>. </p>
					</div>
					<a href="profile_form.php?pro_id=<?php echo $arrpro['pro_id']; ?>" class="more-link button">อ่านต่อ</a>
				</article> <!-- .post -->
			<?php } ?>
							</div>
							<div class="col-md-3 col-md-offset-1">
								<aside class="sidebar">
									<div class="widget wow fadeInRight">
										<h3 class="widget-title">ประเภทผลงาน</h3>
										<ul class="posts">
											<li>
												<h4 class="entry-title">
													<a href="#" id="showeall" >
													ทั้งหมด
													</a>
												</h4>
											</li>
										</ul>
										<ul class="posts">
											<li>
												<h4 class="entry-title">
													<a href="#" id="show1" >
													งานโครงสร้าง
													</a>
												</h4>
											</li>
										</ul>
										<ul class="posts">
											<li>
												<h4 class="entry-title">
													<a href="#" id="show2" >
													งานต่อเติม
													</a>
												</h4>
											</li>
										</ul>
									</div> <!-- .widget -->
								</aside> <!-- .sidebar -->
							</div>
						</div> <!-- .row -->
					</div> <!-- .container -->
				</div> <!-- .inner-content -->
			</main> <!-- .content -->

			<footer class="site-footer wow fadeInUp">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							
							<div class=" branding">
								<img src="images/logo-footer.png" alt="Site title" class="logo-icon">
								<h1 class="site-title"><a href="#">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></a></h1> 
								<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
							</div> <!-- .branding -->

							<p class="copy">Copyright 2014 บริษัทช่างจ๊อดรับเหมาก่อสร้าง. designed by Themezy. All rights reserved</p>
						</div>
						
						<div class="col-md-6 align-right">
						
							<nav class="footer-navigation">
								<a href="profilenews.php">ผลงาน</a>
								<a href="showjob.php">ตรวจสอบราคางาน</a>
								<a href="contact.php">ติดต่องาน</a>
							</nav> <!-- .footer-navigation -->
							<!--
							<div class="social-links">
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
							</div>  .social-links -->
						
						</div>
					</div>

				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>