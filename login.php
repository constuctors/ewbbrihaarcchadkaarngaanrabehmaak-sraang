<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>เข้าสู่ระบบ</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Open+Sans:600'>

	<link rel="stylesheet" href="css/stylelogin.css">
</head>
<body>
<div class="login-wrap">
	<div class="login-html" style="height: 1050px;">
		<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">เข้าสู่ระบบ</label>
		<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">สมัครสมาชิก</label>
		<div class="login-form">
			<form name="login" method="post" action = "check_login.php">
				<div class="sign-in-htm">
					<div class="group">
						<label>Username</label>
						<input id="user" type="text" name="username_log" class="input" placeholder="Username..">
					</div>
					<div class="group">
						<label>Password</label>
						<input id="pass" type="password" name="pass_log" class="input" data-type="password" placeholder="Password..">
					</div>
					<div class="group">
						<input type="submit" class="button" value="Sign In">
					</div>
				</div>
			</form>
			<form action="../constuctors/User/regis_insert.php" method = "post" enctype="multipart/form-data" class="form-inline">
				<div class="sign-up-htm">
					<div class="group">
						<label>Username</label>
						<input id="user" type="text" class="input"  name="username" placeholder="Username...">
					</div>
					<div class="group">
						<label>Password</label>
						<input id="pass" type="password" class="input" data-type="password" name="password" placeholder="password...">
					</div>
					<div class="group">
						<label>ชื่อ</label>
						<div>
							<input id="user" type="text" name="use_fname" width="100px" placeholder="ชื่อ..." class="input">

						</div>
					</div>
					<div class="group">
						<label >นามสกุล</label>
						<input id="user" type="text" name="use_lname" placeholder="นามสกุล..." class="input">
						
					</div>
					<div class="group">
						<div class="form-group">
							<div class="col-md-6">
								<label>ชื่อเล่น</label>
								<input type="text"  name="use_nname" placeholder="ชื่อเล่น..." class="input">
							</div>
							<div class="col-md-6">
								<label>อายุ</label>
								<input type="text"  name="use_age" placeholder="อายุ..." class="input">
							</div>
						</div>
					</div>
					<div class="group">
						<label>ที่อยู่</label>
						<textarea name="use_address" id="use_address" rows="5"  placeholder="ที่อยู่..." class="input"></textarea>
						
					</div>
					<div class="form-group">
						<label>
							เพศ
						</label>
						<div class="col-md-12">
							<label>
								<input id="radio" type="radio" name="use_sex" value="1" class="radio" checked>
								ชาย
							</label>
							<label>
								<input id="radio" type="radio" name="use_sex" value="2" class="radio">
								หญิง
							</label>
						</div>
					</div>
					<div class="group">
						<label>เลือกรูปภาพ</label>
						<input name="use_image" type="file">
						
					</div>
					<div class="group">
						<input type="submit" class="button" value="ตกลง">
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
  
  
</body>
</html>