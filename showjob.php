<?php 
	ob_start();
	session_start();
 ?>
  <?php 
  require './connect/connecDb.php';
  $query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
  $result = mysqli_query($condb,$query);
  $objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
    
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="style.css">
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

		<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
		<script type="text/javascript" src="./js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="./js/bootstrap.min.js"></script>


	<title>แสดงคำนวนราคาของงาน</title>

	<script>
 	
 		$(function(){
 			$(".rdoclick").on('click',function(evt) { 
           			
 					$.ajax({
									url: 'calculatejob.php',
				 					type: "POST",
				 					data : { id : $(this).val() } ,
				 					success : function(result){
									$("#showfrom").html(result);
							}
						});
			 		})
			});
 	</script>
</head>
	<body>
		
		<div id="site-content">
			
			<header class="site-header">
				<div class="top-header">
					<div class="container">
						<a href="tel:80049123441">Call Us: 086-478-1761</a>
						
						<nav class="member-navigation pull-right">
							<?php  if($_SESSION['use_id'] != "")
							{
							?>
								<a href="detailuser.php"><i class="fa fa-user"></i>
									<?php echo $objresult['use_fname'];echo "&nbsp";echo $objresult['use_lname'];?>
								</a>
								<a href="logout.php"><i class="fa fa-lock"></i> ออกจากระบบ</a>
							<?php }else{ ?>
							<a href="fromLogin.php"><i class="fa fa-user"></i> Register</a>	
							<a href="fromLogin.php"><i class="fa fa-lock"></i> Login</a>
							<?php } ?>
						</nav> <!-- .member-navigation -->
					</div> <!-- .container -->
				</div> <!-- .top-header -->

				<div class="bottom-header">
					<div class="container">
						<a href="index.php" class="branding pull-left">
							<img src="images/logo-icon.png" alt="Site title" class="logo-icon">
							<h1 class="site-title">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></h1> 
							<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
						</a> <!-- #branding -->
						
						<nav class="main-navigation pull-right">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item"><a href="profilenews.php">ผลงาน</a></li>
								<li class="menu-item"><a href="showjob.php">ตรวจสอบราคางาน</a></li>
								<li class="menu-item"><a href="contact.php">ติดต่องาน</a></li>
							</ul>
						</nav> <!-- .main-navigation -->
					</div> <!-- .container -->
				</div> <!-- .bottom-header -->
			</header> <!-- .site-header -->
			<main class="content" style="height: 500px;">
				<div class="breadcrumbs">
					<div class="container">
						<a href="index.php">หน้าแรก</a> &rarr;
						<a href="showjob.php">ตรวจสอบราคางาน</a>
					</div>
				</div>

				<div class="page-content">
			<div class="row">
				<div class="col-md-2">
					
				</div>

				<div class="col-md-8">
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#category1" aria-controls="home" role="tab" data-toggle="tab">
								งานโครงสร้าง
							</a>
						</li>
						<li role="presentation">
							<a href="#category2" aria-controls="home" role="tab" data-toggle="tab">
								งานต่อเติม
							</a>
						</li>
					</ul>
					<form id="stable" class="form-horizontal" method="post">
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="category1" >
							<?php 
								$selec = "select * from getjob,category where getjob.cate_id = category.cate_id and getjob.cate_id =1";
								$rsl = mysqli_query($condb,$selec);
								while ($rowjob = mysqli_fetch_array($rsl)) {
							 ?>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label class="radio-inline col-md-12 col-sm-6 col-xs-12" >
									<div class="col-md-3 col-sm-3 col-xs-3">
										<input type="radio" class="rdoclick" name="get_id" value="<?php echo $rowjob['get_id']; ?>">
									</div>
									<div class="col-md-9 col-sm-9 col-xs-9" >
										<?php echo $rowjob['get_name']; ?>
									</div>
								</label>		
							</div>
								
							 <?php } ?>
						</div>

						<div role="tabpanel" class="tab-pane" id="category2" >
							<?php 
								$selec = "select * from getjob,category where getjob.cate_id = category.cate_id and getjob.cate_id =2";
								$rsl = mysqli_query($condb,$selec);
								while ($rowjob = mysqli_fetch_array($rsl)) {
							 ?>
							<div class="col-md-3 col-sm-6 col-xs-12">
								<label class="radio-inline col-md-12 col-sm-6 col-xs-12" >
									<div class="col-md-3 col-sm-3 col-xs-3">
										<input type="radio" class="rdoclick" name="get_id" value="<?php echo $rowjob['get_id']; ?>">
									</div>
									<div class="col-md-9 col-sm-9 col-xs-9" >
										<?php echo $rowjob['get_name']; ?>
									</div>
								</label>		
							</div>
							 <?php } ?>
						</div>
					</div>
					</form>
				</div>

				<div class="col-md-2">
					
				</div>
			</div>
			<br>
			<form class="showfrom">
				<div class="row">
					<div class="col-md-2">
						
					</div>

					<div class="col-md-8"  id="showfrom">
						
					</div>

					<div class="col-md-2">
						
					</div>
				</div>
			</form>
					
				
					
				</div> <!-- .inner-content -->
			</main> <!-- .content -->
			

			<footer class="site-footer wow fadeInUp">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							
							<div class=" branding">
								<img src="images/logo-footer.png" alt="Site title" class="logo-icon">
								<h1 class="site-title"><a href="#">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></a></h1> 
								<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
							</div> <!-- .branding -->

							<p class="copy">Copyright 2014 บริษัทช่างจ๊อดรับเหมาก่อสร้าง. designed by Themezy. All rights reserved</p>
						</div>
						
						<div class="col-md-6 align-right">
						
							<nav class="footer-navigation">
								<a href="profilenews.php">ผลงาน</a>
								<a href="showjob.php">ตรวจสอบราคางาน</a>
								<a href="contact.php">ติดต่องาน</a>
							</nav> <!-- .footer-navigation -->
							<!--
							<div class="social-links">
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
							</div>  .social-links -->
						
						</div>
					</div>

				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>
</html>