
 			<form method="POST" class="form-horizontal" action="session_contact.php">
 				<div class="col-md-12" style="background-color: #F5F5F5; margin:10px; padding: 20px;">
 					<div class="row">
						<div class="col-md-12">
							<h2 style="text-align: center;margin: 15px;">ข้อมูลส่วนตัว</h2>
							<div class="col-md-3 col" >
								<div class="row"> 
									<div class="col-md-12">
										<img src="picture/<?php echo $objresult['use_image']; ?>" style="width:100%; " class="img-rounded" >
									</div>
								</div>
							</div>
							
							<div class="col-md-9">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<div class="col-md-3">
												<label  for="exampleInputName2 " style="font-size: 16px; text-align:center;">ชื่อ - นามสกุล:</label>
											</div>
											<div class="col-md-9">
												<input type="text" class="form-control col-xs-4" readonly value="<?php echo $objresult['use_fname']."&nbsp;".$objresult['use_lname'];?>">
											</div>
											
											
										</div>
									</div>

									<div class="col-md-12">
										<div class="form-group">

											<div class="col-md-3" style="text-align:center;">
												<label  for="exampleInputName2 col-xs-2" readonly style="font-size: 16px;">อายุ:</label>
											</div>
											<div class="col-md-2">
												<input type="text" class="form-control col-xs-4" readonly value="<?php echo $objresult['use_age'];?>">
											</div>
											<div class="col-md-3" style="text-align:center;">
												<label  for="exampleInputName2 col-xs2" style="font-size: 16px;">ชื่อเล่น:</label>
											</div>
											<div class="col-md-4">
												<input type="text" class="form-control col-xs-4" readonly value="<?php echo $objresult['use_nname'];?>">
											</div>
										</div>
									</div>
									
									<div class="col-md-12" style="text-align: left;">
										<div class="form-group" >
											<div class="col-md-12">
												<label  for="exampleInputName2 col-xs2" style="font-size: 16px;">ที่อยู่:</label>
											</div>
											<div class="col-md-12">
												<textarea class="form-control" rows="10"  readonly name="use_address" id="use_address"><?php echo $objresult['use_address']; ?>
												</textarea>
											</div>
										</div>
									</div>
									<div class="col-md-12" style="text-align: center;">
										<a href="editdetailuser.php?use_id=<?php echo $objresult['use_id']; ?>">
											<button type="button" class="btn btn-info">แก้ไขข้อมูลส่วนตัว</button>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
 				</div>
			</form>
			<div class="col-md-12" style="background-color: #F5F5F5; border-radius:5px;padding: 6px; margin:10px; ">
				<h2 style="margin: 10px;">ข้อมูลจ้างงาน</h2>

				<table class="table table-bordered table-striped" style="text-align: center;">
					<thead >
						<th style="text-align: center;">
							วันที่ติดต่องาน
						</th >
						<th style="text-align: center;">
							สเตตัส
						</th>
						<th style="text-align: center;">
							สถานที่
						</th>
						<th style="text-align: center;">
							ราคางาน
						</th>
						<th style="text-align: center;">
							รายละเอียด
						</th>
					</thead>
					<?php 
						$qr = "		select  con_id , user.use_fname as fname 
									, user.use_fname as lname 
									, jb.con_datetime as datetime 
									, cn.const_name as statusname
									, con_address as address
									, con_totalprice as price 
									, jb.con_status as statusid
								from user,jobcontact as jb,constatus as cn
								where jb.con_status = cn.const_id 
									and user.use_id = jb.use_id 
									and jb.use_id  = ' ".$objresult['use_id']." '
							";
						$show = mysqli_query($condb,$qr);
						while ($row = mysqli_fetch_array($show)) 
						{
					 ?>
					 <tr>
						<input type="hidden" name="con_id" value="<?php echo $row['con_id']; ?>">
						<td>
							<?php echo $row['datetime']; ?>
						</td>
						<?php if ($row['statusid'] == 0) 
							{
						?>
						<td style="color: #FE9A2E;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php 	} ?>

						<?php if ($row['statusid'] == 1) 
							{
						?>
						<td style="color: #2EFE2E;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>
						<?php if ($row['statusid'] == 2) 
							{
						?>
						<td style="color: red;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>

						<?php if ($row['statusid'] == 4) 
							{
						?>
						<td style="color: #2E2EFE;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>
						<td>
							<?php echo $row['address']; ?>
						</td>
						<td>
							<?php echo $row['price']; ?>
						</td>
						<td>
							<a href="detailjob.php?id=<?php echo $row['con_id']; ?>">
								<button type="button" class="btn btn-info">รายละเอียด</button>
							</a>
								
							
						</td>
						<?php } ?>
					</tr>
				</table>	

				<div class="col-md-12" style="height: 20px;">
				</div>
			</div>