<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='index.php';";
		echo "</script>";
		exit();
	}
	date_default_timezone_set("Asia/Bangkok");

	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	$con_id = $_POST['con_id'];
	$qr = "		select *
			from user,jobcontact as jb,constatus as cn
			where jb.con_status = cn.const_id 
			and user.use_id = jb.use_id 
			and con_id = ' ".$con_id. " ' ";
	$show = mysqli_query($condb,$qr);
	$detail = mysqli_fetch_array($show,MYSQLI_ASSOC); 	
		
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายละเอียดของงาน</title>

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../js/datatable/css/jquery.dataTables.css">
	<!-- jQuery -->
	<script type="text/javascript" src="../js/datatable/js/jquery-1.10.2.min.js"></script>
	 <!-- DataTables -->
	<script type="text/javascript" src="../js/datatable/js/jquery.dataTables.js"></script>
	
	<script>
		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').focus()
		});	
		$(document).ready(function() {
			$('#table').DataTable();
		} );
</script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: #3498DB;>
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<h2 style="text-align: center;">เช็คชื่อผู้มาทำงาน</h2>
			<?php 
						if ($_GET['url'] == "") 
						{
							include_once("allTechnician.php"); 
						}else{
							include_once( $_GET['url']); 
						}
			?>
		</div>
	
	</div>
</div>

</body>
</html>