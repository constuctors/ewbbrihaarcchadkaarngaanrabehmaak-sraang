<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "Please Login!";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="th">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>เพิ่มข้อมูลพนักงาน</title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>

    	<script>
			$(document).ready(function() 
			{
				$("#showprovince").change(function()
				{
					$.ajax({
						url: 'searchpro.php',
						type: "GET",
						data: { id : $(this).val() } ,
						success : function(result){
									$("#showarea").html(result);
									
							}
					})
				});
			});
		</script>
    
	</head>


	<body>
		
<nav class="navbar navbar-default navbar-fixed-top navbar " style="background-color: #3498DB; ">
	<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<br>	<br>	<br>	<br>	
<div class="container">
	<div class="row">	
		<div class="col-md-8 col-md-offset-2"  style="background-color: #F5F5F5;  border-radius: 5px; padding: 10px;">
			<h2 style="text-align: center;">สมัครพนักงาน</h2>
			<p>
				<form action="addP_Insert.php" class="form-horizontal" name="addPersonal" method = "post" enctype="multipart/form-data">

					<div class="contact-form ">
						<div class="form-group ">
							<label class="col-sm-2 control-label">Username :</label>
							<div class="col-sm-10">
								<input type="text" name="username" class="form-control"  placeholder="Username..">
							</div>
						 </div>
						 <div class="form-group ">
							<label class="col-sm-2 control-label">Password :</label>
							<div class="col-sm-10">
								<input type="password" name="password" class="form-control" placeholder="Password..">
							</div>
						 </div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ชื่อ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_fname" class="form-control" placeholder="ชื่อ..">
							</div>

							<label class="col-sm-2 control-label">นามสกุล :</label>
							<div class="col-sm-4">
								<input type="text" name="use_lname" class="form-control" placeholder="นามสกุล..">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ชื่อเล่่น :</label>
							<div class="col-sm-4">
								<input type="text" name="use_nname" class="form-control" placeholder="ขื่อเล่น..">
							</div>

							<label class="col-sm-2 control-label">อายุ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_age" class="form-control" placeholder="อายุ....">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">วัน/เดือน/ปีเกิด (01/02/1990) :</label>
							<div class="col-sm-4">
								<input type="text" name="use_birthday" class="form-control" placeholder="วัน/เดือน/ปีเกิด....">
							</div>
							<label class="col-sm-2 control-label">เบอร์โทรศํพท์ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_phone" class="form-control" placeholder="เบอร์โทรศํพท์....">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ที่อยู่ :</label>
							<div class="col-sm-10">
								<textarea name="use_address" class="form-control" rows="3px" placeholder="ที่อยู่....."></textarea>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">จังหวัด :</label>
							<div class="col-sm-4">
								<select id="showprovince" name="PROVINCE_ID" class="form-control">
									<option value="">
									=====กรุณาเลือกจังหวัด=====
									</option>
										<?php 
											$slpr = 	" 	select *
															from province
														";
											$qr = mysqli_query($condb,$slpr);
											while ($arr = mysqli_fetch_array($qr)) 
											{
										 ?>
									
									<option value="<?php echo $arr['PROVINCE_ID']; ?>">	
										<?php echo $arr['PROVINCE_NAME']; ?>
									</option>
									<?php } ?>
									
								</select>
							</div>
							<label class="col-sm-2 control-label">อำเภอ / แขวง :</label>
							<div class="col-sm-4" id="showarea">
								
							</div>
						</div>
						
						<div class="form-group ">
							<label class="col-sm-2 control-label">ตำบล / เขต :</label>
							<div class="col-sm-4" id="showsubarea">	

							</div>

							<label class="col-sm-2 control-label">รหัสไปรษณีย์ :</label>
							<div class="col-sm-4" id="shownuma">
								<input type="text" name="use_postalcode" id="zipcode" class="form-control" placeholder="รหัสไปรษณีย์....">
							</div>
						</div>
						
						<div class="form-group ">
							<label class="col-sm-2 control-label">สัญชาติ :</label>
							<div class="col-sm-2">
								<input type="text" name="use_nationality" class="form-control" placeholder="สัญชาติ....">
							</div>
							<label class="col-sm-2 control-label">เชื้อชาติ :</label>
							<div class="col-sm-2">
								<input type="text" name="use_race" class="form-control" placeholder="เชื้อชาติ....">
							</div>
							<label class="col-sm-2 control-label">ศาสนา :</label>
							<div class="col-sm-2">
								<input type="text" name="use_religion" class="form-control" placeholder="ศาสนา....">
							</div>
						</div>

						
						<div class="form-group ">
							<label class="col-sm-2 col-xs-3 control-label">เพศ :</label>
							<label class="radio-inline col-sm-1 col-xs-3">
								<input type="radio" name="use_sex" value="1" checked style="width: 20px">
								ชาย
							</label>
							<label class="radio-inline col-sm-1 col-xs-3">
								<input type="radio" name="use_sex" value="2" style="width: 20px">
								หญิง
							</label>
							<label class="col-sm-2 col-xs-12 control-label">ตำแหน่งงาน :</label>
							<div class="col-sm-5 col-xs-12">
								<select name="off_id" class="form-control">
								<option value=""><--เลือกตำแหน่ง--></option>
									<?php 
										$sel = "select * from office";
										$res = mysqli_query($condb,$sel);
										while ( $row = mysqli_fetch_array($res)) 
										{
									?>
								<option style="color: black;" value="<?php echo $row["off_id"]; ?>">
									<?php echo $row["off_name"]; 
										}
									?>
								</option>
							</select>
							</div>

						</div>
						<div class="form-group ">
							<label class="col-sm-2 col-xs-12 control-label">
								ระดับการศึกษา :
							</label>	
							<div class="col-sm-4 col-xs-12">
								<select name="edu_id" class="form-control">
									<option value=""><--เลือกระดับการศึกษา--></option>
										<?php 
											$sel = "select * from education";
											$res = mysqli_query($condb,$sel);
											while ( $row = mysqli_fetch_array($res)) 
											{
										?>
									<option style="color: black;" value="<?php echo $row["edu_id"]; ?>">
										<?php echo $row["edu_name"]; 
											}
										?>
									</option>
								</select>
							</div>
							<label class="col-sm-2 col-xs-12 control-label">ค่าแรงรายวัน :</label>
							<div class="col-sm-4 col-xs-12">
								<input type="text" name="use_money" class="form-control" placeholder="บาท..">
							</div>
							<label class="col-sm-1 control-label">บาท</label>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">เลือกรูปภาพ :</label>
							<div class="col-sm-4">
								<input name="use_image"  type="file" style="width:400px" >
							</div>
						</div>
					</div>

					<div class="col-md-12" style="text-align: center;">
						<button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
						<a href="indexA.php"><button type="button" class="btn btn-danger">ย้อนกลับ</button></a>
					</div>
						
				</form>
			</p>
		</div>	
	</div>
</div>
		
</body>
</html>