<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "Please Login!";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo " คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายชื่อผู้ติดต่องาน</title>

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top navbar " style="background-color: #3498DB; ">
	<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<br>	<br>	<br>	<br>	
<div class="container">
	<div class="row">

	<div class="col-md-12"><a href="indexA.php"><button type="button" class="btn btn-danger">ย้อนกลับ</button></a></div>

	<div class="col-md-1"></div>
	
	<div class="col-md-10">
		<h3>รายชื่องานที่ปฏิเสธ</h3>
		<div>
			<table class="table table-bordered table-striped" style="text-align: center;">
				<thead >
					<th style="text-align: center;">
						ชื่อลูกค้า
					</th >
					<th style="text-align: center;">
						วันที่ติดต่องาน
					</th >
					<th style="text-align: center;">
						สเตตัส
					</th>
					<th style="text-align: center;">
						รายละเอียด
					</th>
				</thead>
				<?php 
					$qr = "		select  con_id,user.use_fname as fname , user.use_fname as lname 
							, jb.con_datetime as datetime , cn.const_name as status
							from user,jobcontact as jb,constatus as cn
							where jb.con_status = cn.const_id and user.use_id = jb.use_id and  jb.con_status = '2' ";
					$show = mysqli_query($condb,$qr);
					while ($row = mysqli_fetch_array($show)) 
					{
				 ?>
				<tr>

				<form action="detailjobcontact.php" method="post">
					<td>
						<input type="hidden" name="con_id" value="<?php echo $row['con_id']; ?>">
						<?php echo $row['fname']."&nbsp;".$row['lname']; ?>
					</td>
					<td>
						<?php echo $row['datetime']; ?>
					</td>
					<td>
						<?php echo $row['status']; ?>
					</td>
					<td>
						<button type="submit" class="btn btn-primary">รายละเอียด</button>
					</td>
				</form>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>

	<div class="col-md-1"></div>	

	</div>

</div>
</body>
</html>