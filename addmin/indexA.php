<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
	
	//echo $objresult["use_fname"];	

	$selectjc = 	" 	select * 
				from jobcontact
				where con_status = 0
			";
	$qrjc = mysqli_query($condb,$selectjc);
	while ($arrjc = mysqli_fetch_array($qrjc)) 
	{
		
		$i = $i+1;
	}
		
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผู้ดูแลระบบ<?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		
	</head>


	<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->
<div class="container" >	
	<div class="row" style="background-color: #F5F5F5; padding: 10px 0px 10px 0px;">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<h2 class="section-title" style="color: #3366FF;"><center>จัดการระบบ</center></h2>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="addPersonal.php">
						<figure class="project-thumbnail"><img src="../images/user.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">สมัครพนักงาน</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="showPersonal.php">
						<figure class="project-thumbnail"><img src="../images/userprofile.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">ข้อมูลพนักงาน</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="checkPersonal.php">
						<figure class="project-thumbnail"><img src="../images/checkPersonal.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">เช็คชื่อพนักงาน</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="addgetjob.php">
						<figure class="project-thumbnail"><img src="../images/addgetjob.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">เพิ่มงานที่รับเหมา</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="form_salary.php">
						<figure class="project-thumbnail"><img src="../images/salary.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">ออกเงินเดือนพนักงาน</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="showuser.php">
						<figure class="project-thumbnail"><img src="../images/usercontact.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">
						รายชื่อผู้มาติดต่องาน
						<span class="badge" style="background-color: green;"><?php echo $i; ?></span>
					</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="show_contact.php">
						<figure class="project-thumbnail"><img src="../images/contact.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">รายชื่องานที่รับ</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="show_notcontact.php">
						<figure class="project-thumbnail"><img src="../images/strategy.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">รายชื่องานที่ปฏิเสธ</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="project" align="center">
					<a href="user.php">
						<figure class="project-thumbnail"><img src="../images/resume.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">ประวัติลูกค้า</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="profile_form" align="center">
					<a href="profile_form.php">
						<figure class="project-thumbnail"><img src="../images/profile.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">ผลงาน</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="report_form" align="center">
					<a href="report_form.php">
						<figure class="project-thumbnail"><img src="../images/report.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">รายงานต่างๆ</h3>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div class="report_form" align="center">
					<a href="profit_form.php">
						<figure class="project-thumbnail"><img src="../images/lucre.png" alt="Project 1" width="150" height="150"></figure>
					</a>
					<h3 class="project-title">รายงานผลกำไรของบริษัท</h3>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>