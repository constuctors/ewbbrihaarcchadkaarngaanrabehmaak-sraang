<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='index.php';";
		echo "</script>";
		exit();
	}
	date_default_timezone_set("Asia/Bangkok");

	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายชื่อลูกค้า</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: #3498DB;>
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-2">
			<div class="col-md-12">
				<a href="user.php">
					<button type="button" class="btn btn-danger">ย้อนกลับ</button>
				</a>
			</div>
		</div>

		<div class="col-md-8">
			<div class="col-md-12" style="background-color: #E6E6FA; border-radius:5px;padding: 6px;">
				<h2 style="margin: 10px; text-align: center;">ข้อมูลลูกค้า</h2>

				<?php 
					$use_id = $_GET['use_id'];
					$selectuser = 	"	select *
									from user
									where use_id = ' ".$use_id." '
								";
					$qruser = mysqli_query($condb,$selectuser);
					$objuser = mysqli_fetch_array($qruser,MYSQLI_ASSOC);
				 ?>
				 <form method="POST" class="form-horizontal" action="session_contact.php">
	 				<div class="col-md-12" style=" padding: 20px;background-color: #E6E6FA;">
	 					<div class="row">
							<div class="col-md-12">
								<div class="col-md-3" >
									<div class="row">
										<div class="col-md-12">
											<img src="../picture/<?php echo $objuser['use_image']; ?>" style="width:100%; " class="img-rounded" >
										</div>
									</div>
								</div>
								
								<div class="col-md-9">
									<div class="row">
										<div class="form-group">
											<label  for="exampleInputName2 " class="col-sm-3 control-label">
												ชื่อ - นามสกุล:
											</label>
											<div class="col-md-9">
												<input type="text" class="form-control" readonly value="<?php echo $objuser['use_fname']."&nbsp;".$objuser['use_lname'];?>">
											</div>
										</div>
										<div class="form-group">

											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												อายุ :
											</label>
											<div class="col-md-2">
												<input type="text" class="form-control" readonly value="<?php echo $objuser['use_age'];?>">
											</div>
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												ชื่อเล่น :
											</label>
											<div class="col-md-4">
												<input type="text" class="form-control" readonly  value="<?php echo $objuser['use_nname'];?>">
											</div>
										</div>
										<div class="form-group">
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												เบอร์โทรศัพท์ :
											</label>
											<div class="col-md-4">
												<input type="text" class="form-control" readonly  value="<?php echo $objuser['use_phone'];?>">
											</div>
										</div>
										<div class="form-group">
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												ที่อยู่ :
											</label>
											<div class="col-md-9">
												<textarea class="form-control"  readonly  name="use_address" id="use_address"><?php echo $objuser['use_address']; ?>
												</textarea>
											</div>
										</div>
										<div class="form-group">
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												ตำบล/แขวง :
											</label>
											<div class="col-md-3">
												<input type="text" class="form-control" readonly  value="<?php echo $objuser['use_subarea'];?>">
											</div>
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												อำเภอ/เขต :
											</label>
											<div class="col-md-3">
												<input type="text" class="form-control" readonly  value="<?php echo $objuser['use_area'];?>">
											</div>
										</div>
										<div class="form-group">
											<label  for="exampleInputName2" class="col-sm-3 control-label" >
												จังหวัด :
											</label>
											<div class="col-md-3">
												<input type="text" class="form-control" readonly  value="<?php echo $objuser['use_province'];?>">
											</div>
										</div>
					
									</div>
								</div>
							</div>
						</div>
	 				</div>
				</form>

				<div class="col-md-12" style="height: 20px;">
				</div>
			</div>


			<div class="col-md-12" style="background-color: #E0F2F7; border-radius:5px;padding: 6px; margin-top:10px; ">
				<h2 style="margin: 10px;">ข้อมูลจ้างงาน</h2>

				<table class="table table-bordered table-striped" style="text-align: center;">
					<thead >
						<th style="text-align: center;">
							วันที่ติดต่องาน
						</th >
						<th style="text-align: center;">
							สเตตัส
						</th>
						<th style="text-align: center;">
							รายละเอียด
						</th>
					</thead>
					<?php 
						$qr = "		select  con_id , user.use_fname as fname , user.use_fname as lname 
									, jb.con_datetime as datetime , cn.const_name as statusname , jb.con_status as statusid
								from user,jobcontact as jb,constatus as cn
								where jb.con_status = cn.const_id 
									and user.use_id = jb.use_id 
									and jb.use_id  = $use_id
							";
						$show = mysqli_query($condb,$qr);
						while ($row = mysqli_fetch_array($show)) 
						{
							if ($row['statusid'] != "") 
							{
					 ?>
					 <tr>

					<form action="detailjobcontact.php" method="post">
						<input type="hidden" name="con_id" value="<?php echo $row['con_id']; ?>">
						<td>
							<?php echo $row['datetime']; ?>
						</td>
						<?php if ($row['statusid'] == 0) 
							{
						?>
						<td style="color: #FE9A2E;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php 	} ?>

						<?php if ($row['statusid'] == 1) 
							{
						?>
						<td style="color: #2EFE2E;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>
						<?php if ($row['statusid'] == 2) 
							{
						?>
						<td style="color: red;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>

						<?php if ($row['statusid'] == 4) 
							{
						?>
						<td style="color: #2E2EFE;">
							<b><?php echo $row['statusname']; ?></b>
						</td>
						<?php } ?>
						<td>
							<button type="submit" class="btn btn-primary">รายละเอียด</button>
						</td>
					</form>
					</tr>
					<?php }else { ?>
					<tr style="text-align: center;">
						<td></td>
						<td style="color: #FE9A2E;">
							<b>ยังไม่มีการจ้างงาน</b>
						</td>
						<td></td>
					</tr>
					
					<?php }
					} ?>
				</table>
				

				<div class="col-md-12" style="height: 20px;">
				</div>
			</div>
		</div>

		
		<div class="col-md-2"></div>
	</div><!-- close row-->
</div><!-- close container-->

</body>
</html>