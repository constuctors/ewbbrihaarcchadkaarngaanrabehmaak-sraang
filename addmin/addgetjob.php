<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ!');window.location='../index.php';";
		echo "</script>";
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>เพิ่มงานที่รับเหมา</title>

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../js/datatable/css/jquery.dataTables.css">
	<!-- jQuery -->
	<script type="text/javascript" src="../js/datatable/js/jquery.js"></script>
	 <!-- DataTables -->
	<script type="text/javascript" src="../js/datatable/js/jquery.dataTables.js"></script>


	<style>
 		.body
 		{
			background: #CCFFFF;
			margin: 10px;
 		}

 	</style>
 	<script>
 		$(document).ready(function() {
			$('#table').DataTable();
		});
	 		
 	</script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top navbar " style="background-color: #3498DB; ">
	<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<br>	<br>	<br>	<br>

<div class="container">
	<div class="row">

	<div class="col-md-12"><a href="indexA.php"><button type="button" class="btn btn-danger">ย้อนกลับ</button></a></div>

	<div class="col-md-9 col-md-offset-1" style="background-color:#F5F5F5; ">
		<form action="addG_Insert.php" method="post">
			<h2>เพิ่มงานที่รับเหมา</h2>
		 	<table width="100%"  class="body table" >
					<tr>
						<td>ชื่องาน</td>
						<td>
							 <input type="text"  class="form-control" name="get_name"  id="get_name" placeholder="ชื่องาน...">
						</td>
					</tr>
					<tr>
						<td>ขนาดของพื้นที่</td>
						<td>
							 <input type="text"  class="form-control" name="get_scale"  id="get_scale" placeholder="ขนาดพื้นที่...">	 
						</td>
						<td>/ราคา<input type="text"  class="form-control" name="get_money"  id="get_money" placeholder="ราคา..."></td>
					</tr>
					<tr>
						<td>
						
							<select name="cate_id" class="form-control">
								<option value=""><----ประเภทงาน----></option>
						<?php 
							$scate = "select * from category";
							$se = mysqli_query($condb,$scate);
							while ($sele = mysqli_fetch_array($se)) 
							{
						 ?>
								<option value="<?php echo $sele['cate_id']; ?>">
									<?php echo $sele['cate_name']; 
							}
						?>
								</option>
							</select>
						</td>
					</tr>
			</table>
				<div style="text-align: center;" class="col-md-12"> 
					<input type="submit" class="btn btn-success" value="บันทึก">
				</div>
		 </form>

		 	<div class="col-md-12">
					<h3>รายชื่องาน</h3>
			</div>
		
			<table class="table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<td style="text-align:center;">ชื่อ </td>
						<td style="text-align:center;">ขนาดพื่นที่ </td>
						<td style="text-align:center;">ราคา </td>
						<td style="text-align:center;">ประเภท </td>
						<td style="text-align:center;">แก้ไข</td>
					</tr>
				</thead>
					
				<tbody>
				<?php 
					$show = 	"	select * 
								from getjob,category
								where getjob.cate_id = category.cate_id
							";
					$qr = mysqli_query($condb,$show);
					while ($ar = mysqli_fetch_array($qr)) 
					{
						$i = $i+1;
				 ?>
					 
				 	<tr>
						<td style="text-align:center;">
							<?php echo $ar['get_name']; ?>
						</td>
						<td  style="text-align:center;">
							<?php echo $ar['get_scale']; ?>
						</td>
						<td style="text-align:center;">
							<?php echo $ar['get_money']; ?>		
						</td>
						<td style="text-align:center;">
							<?php echo $ar['cate_name']; ?>
						</td>
						<td style="text-align:center;">
							<button type="button" id="btnup<?php echo $i?>" class="btn btn-info">แก้ไข</button>
						</td>
					</tr>
				<form id="updatejob<?php echo $i ?>" action="update_job.php" method="post">
					<tr  id="trshow<?php echo $i?>" style="display: none;">
							<input type="hidden" name="get_id" value="<?php echo $ar['get_id'] ?>">
						<td>
							<input type="text" name="get_name" class="form-control" value="<?php echo $ar['get_name']; ?>">
						</td>
						<td>
							<input type="text" name="get_scale" class="form-control" value="<?php echo $ar['get_scale']; ?>">		
						</td>
						<td>
							<input type="text" name="get_money" class="form-control" value="<?php echo $ar['get_money']; ?>">
						</td>
						<td>
							<select name="cate_id" class="form-control">
								<?php 
									$scate = "select * from category order by cate_id = ' ".$ar['cate_id']." ' DESC";
									$se = mysqli_query($condb,$scate);
									while ($sele = mysqli_fetch_array($se)) 
									{
								 ?>
										<option value="<?php echo $sele['cate_id']; ?>">
											<?php echo $sele['cate_name']; 
									}
								?>
								</option>
							</select>
						</td>
						<td align="center" style="padding:5px;">
							<button type="submit"  class="btn btn-success">ยืนยัน</button>
							<button type="button" id="btncancel<?php echo $i?>" class="btn btn-danger">ยกเลิก</button>

						</td>
						
					</tr>
				</form>
					<script>
						$(document).ready(function(){
							$("#btnup<?php echo $i?>").click(function(){
						 		$("#trshow<?php echo $i?>").show(700);
						 		
					 		});	
					 		$("#btncancel<?php echo $i?>").click(function(){
					 			$("#trshow<?php echo $i?>").hide(700);
					 		});
						});
						
					 		
					</script>
				<?php } ?>
				</tbody>
			</table>
			<div id="succs">
				
			</div>
		
	</div>
	
	</div>
</div>
 </html>
