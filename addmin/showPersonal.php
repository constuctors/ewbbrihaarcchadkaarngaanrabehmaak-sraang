<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "Please Login!";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo " คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายละเอียดของงาน</title>

	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<!-- font-awesome -->
	<link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../js/datatable/css/jquery.dataTables.css">
	<script type="text/javascript" src="../js/datatable/js/jquery.dataTables.js"></script>
	<script>
		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').focus()
		});	

		$(document).ready(function() {
			$('#table').DataTable();
		} );
	</script>
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top navbar " style="background-color: #3498DB; ">
	<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>
	<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<br>	<br>	<br>	<br>	
<div class="container">
	<div class="row">
		<div class="col-md-2">
			<a href="indexA.php"><button type="button" class="btn btn-danger">ย้อนกลับ</button></a>
		</div>
	
		<div class="col-md-8">
			<table border="1" width="100%"  id="table" height = "60" class="table table-striped table-bordered">
				<thead align="center">
					<th>รูป</td>
					<th >ชื่อ-นามสกุล</td>
					<th>อายุ</td>
					<th>เพศ</td>
					<th>แก้ไข</td>
				</thead>
				<?php 
					$sql = "select *  from user where status_id = 3";
					$qr = mysqli_query($condb,$sql);
					while ( $row = mysqli_fetch_array($qr)) 
					{	
				 ?>
				<tr align="center">
					<td>
						<img src="../picture/<?php echo $row['use_image']; ?>"  width = "100" hieght = "100">
					</td>
					<td>	
						<?php echo $row['use_fname']; ?> &nbsp;
						<?php echo $row['use_lname']; ?>
					</td>
					<td>
						<?php echo $row['use_age']; ?>
					</td>
					<td>
						<?php  if ($row['use_sex'] == 1) {
							$row['use_sex'] = ชาย;
							}else{
								$row['use_sex'] = หญิง;
							}
						echo $row['use_sex']; ?>
							
					</td>
					<td width="30px" height="40px">
						<a href="editPersonal.php?use_id=<?php echo $row['use_id']; ?>">
							<i class="fa fa-cogs fa-3x" aria-hidden="true"></i>
						</a>
					</td>
				</tr>	
				<?php 
					}
				?>
			</table>
		</div>
	
		<div class="col-md-2"></div>
	
	</div>
</div>

</body>
</html>