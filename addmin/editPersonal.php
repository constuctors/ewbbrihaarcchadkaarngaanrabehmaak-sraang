<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}

	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผู้ดูแลระบบ<?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function() 
			{
				$("#showprovince").change(function()
				{
					$.ajax({
						url: 'searchpro.php',
						type: "GET",
						data: { id : $(this).val() } ,
						success : function(result){
									$("#showarea").html(result);
									
							}
					})
				});
			});
		</script>

	</head>


	<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;>
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->
<div class="container" >
	<a href="showPersonal.php"><button type="button" class="btn btn-danger">ยกเลิกการแก้ไข</button></a>
		<div class="row" >
			<?php 
				$id = $_REQUEST['use_id'];
				
				$s_id =  	"	select * 
								from user,office
								where 	use_id = '".$id."'
										and user.off_id = office.off_id 
							";
				$qr_id = mysqli_query($condb,$s_id)or die(mysqli_error($condb));
				$objqr_id = mysqli_fetch_array($qr_id,MYSQLI_ASSOC);
			 ?>
			
			
			<div class="col-md-8 col-md-offset-2"  style="background-color: #F5F5F5;  border-radius: 5px; padding: 10px;">

			<h2 style="text-align: center;">แก้ไขข้อมูลพนักงาน</h2>

				<form action="editP_update.php"  class="form-horizontal" method = "post" enctype="multipart/form-data">

						<div class="contact-form ">
							<input type="hidden" name="use_id" id="use_id" style="width: 300px" readonly="true" value="<?php echo $objqr_id['use_id']; ?>">
							<div class="form-group ">
								<center><img src="../picture/<?php echo $objqr_id['use_image']; ?>"  width = "100" hieght = "100"><br></center>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ชื่อ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_fname" class="form-control" value="<?php echo $objqr_id['use_fname']; ?>">
							</div>

							<label class="col-sm-2 control-label">นามสกุล :</label>
							<div class="col-sm-4">
								<input type="text" name="use_lname"class="form-control" value="<?php echo $objqr_id['use_lname']; ?>">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ชื่อเล่่น :</label>
							<div class="col-sm-4">
								<input type="text" name="use_nname" class="form-control" value="<?php echo $objqr_id['use_nname']; ?>">
							</div>

							<label class="col-sm-2 control-label">อายุ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_age" class="form-control" value="<?php echo $objqr_id['use_age']; ?>">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">วัน/เดือน/ปีเกิด (01/02/1990) :</label>
							<div class="col-sm-4">
								<input type="text" name="use_birthday" class="form-control" value="<?php echo $objqr_id['use_birthday']; ?>">
							</div>
							<label class="col-sm-2 control-label">เบอร์โทรศํพท์ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_phone" class="form-control" value="<?php echo $objqr_id['use_phone']; ?>">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ที่อยู่ :</label><div class="col-sm-10"><textarea name="use_address" class="form-control" rows="3px" ><?php echo $objqr_id['use_address']; ?>
								</textarea>
							</div>
						</div>

						<div class="form-group ">
							<label class="col-sm-2 control-label">จังหวัด :</label>
							<div class="col-sm-4">
								<select id="showprovince" name="PROVINCE_ID" class="form-control">

								<?php 
									
									if ($objqr_id['PROVINCE_ID'] != 0) 
									{
								 ?>
							 		</option>
										<?php 
											$slpr = 	" 	select *
															from province
															order by PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."' desc
														";
											$qr = mysqli_query($condb,$slpr);
											while ($arr = mysqli_fetch_array($qr)) 
											{
										 ?>
									
									<option value="<?php echo $arr['PROVINCE_ID']; ?>">	
										<?php echo $arr['PROVINCE_NAME']; ?>
									</option>
								<?php
											}
									}else
								{
									 ?>
									<option value="">
									=====กรุณาเลือกจังหวัด=====
									</option>
										<?php 
											$slpr = 	" 	select *
															from province
														";
											$qr = mysqli_query($condb,$slpr);
											while ($arr = mysqli_fetch_array($qr)) 
											{
										 ?>
									
									<option value="<?php echo $arr['PROVINCE_ID']; ?>">	
										<?php echo $arr['PROVINCE_NAME']; ?>
									</option>
									<?php }
								}

									 ?>
									
								</select>
							</div>
							
							<label class="col-sm-2 control-label">อำเภอ / แขวง :</label>
							<div class="col-sm-4" id="showarea">
								<select name="AMPHUR_ID" class="form-control" id="ampid">
									<?php 
										if ($objqr_id['AMPHUR_ID'] != 0) 
										{
									 ?>
								 		</option>
											<?php 
												$slam = 	" 	select *
																from amphur
																where PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."'
																order by AMPHUR_ID = ' "  .$objqr_id['AMPHUR_ID']. " ' desc
															";
												$qr = mysqli_query($condb,$slam);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['AMPHUR_ID']; ?>">	
											<?php echo $arr['AMPHUR_NAME']; ?>
										</option>
										<?php
												}
									}else
									{
										 ?>
										<option value="">
										=====กรุณาเลือกอำเภอ=====
										</option>
											<?php 
												$slam = 	" 	select *
																from amphur
															";
												$qr = mysqli_query($condb,$slam);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['AMPHUR_ID']; ?>">	
											<?php echo $arr['AMPHUR_NAME']; ?>
										</option>
										<?php }
									}

										 ?>
								</select>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ตำบล / เขต :</label>
							<div class="col-sm-4" id="showsubarea">	
								<select name="DISTRICT_ID" class="form-control" id="ampid">
									<?php 
										if ($objqr_id['DISTRICT_ID'] != 0) 
										{
									 ?>
								 		</option>
											<?php 
												$slds = 	" 	select *
																from district
																where 	PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."'
																		and AMPHUR_ID = ' ".$objqr_id['AMPHUR_ID']." '
																order by DISTRICT_ID = ' "  .$objqr_id['DISTRICT_ID']. " ' desc
															";
												$qr = mysqli_query($condb,$slds) or die(mysqli_error($condb));
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['DISTRICT_ID']; ?>">	
											<?php echo $arr['DISTRICT_NAME']; ?>
										</option>
										<?php
												}
									}else
									{
										 ?>
										<option value="">
										=====กรุณาเลือกตำบล=====
										</option>
											<?php 
												$slds = 	" 	select *
																from district
															";
												$qr = mysqli_query($condb,$slds);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['DISTRICT_ID']; ?>">	
											<?php echo $arr['DISTRICT_NAME']; ?>
										</option>
										<?php }
									}

										 ?>
								</select>
							</div>
							<label class="col-sm-2 control-label">รหัสไปรษณีย์ :</label>
							<div class="col-sm-4">
								<input type="text" name="use_postalcode" class="form-control" value="<?php echo $objqr_id['use_postalcode']; ?>">
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">สัญชาติ :</label>
							<div class="col-sm-2">
								<input type="text" name="use_nationality" class="form-control" value="<?php echo $objqr_id['use_nationality']; ?>">
							</div>
							<label class="col-sm-2 control-label">เชื้อชาติ :</label>
							<div class="col-sm-2">
								<input type="text" name="use_race" class="form-control" value="<?php echo $objqr_id['use_race']; ?>">
							</div>
							<label class="col-sm-2 control-label">ศาสนา :</label>
							<div class="col-sm-2">
								<input type="text" name="use_religion" class="form-control" value="<?php echo $objqr_id['use_religion']; ?>">
							</div>
						</div>

						
						<div class="form-group ">
							<label class="col-sm-2 col-xs-3 control-label">เพศ :</label>
							<label class="radio-inline col-sm-1 col-xs-3">
								<input type="radio" name="use_sex" value="1" style="width: 20px" <?php if ($objqr_id['use_sex'] == 1) {
										echo "checked";
									}
									?>>
								ชาย
							</label>
							<label class="radio-inline col-sm-1 col-xs-3">
								<input type="radio" name="use_sex" value="2" style="width: 20px" <?php 
									if ($objqr_id['use_sex'] == 2)
									 {
										echo "checked";
									}?>>
								หญิง
							</label>
							<label class="col-sm-2 col-xs-12 control-label">ตำแหน่งงาน :</label>
							<div class="col-sm-5 col-xs-12">
								<select name="off_id" class="form-control">
									<?php 
										$sel = "select * from office ORDER BY off_id = ' ".$objqr_id['off_id']." ' DESC";
										$res = mysqli_query($condb,$sel);
										while ( $row = mysqli_fetch_array($res)) 
											{
									 ?>
									<option value="<?php echo $row["off_id"]; ?>">
									<?php 
											echo $row["off_name"]; 
											}
									?>
									</option>
								</select>
							</div>

						</div>
						<div class="form-group ">
							<label class="col-sm-2 col-xs-12 control-label">
								ระดับการศึกษา :
							</label>	
							<div class="col-sm-4 col-xs-12">
								<select name="edu_id" class="form-control">
									<?php 
										$sel = "select * from education ORDER BY edu_id = ' ".$objqr_id['edu_id']." ' DESC";
										$res = mysqli_query($condb,$sel);
										while ( $row = mysqli_fetch_array($res)) 
											{
									 ?>
									<option value="<?php echo $row["edu_id"]; ?>">
									<?php 
											echo $row["edu_name"]; 
											}
									?>
									</option>
								</select>
							</div>
							<label class="col-sm-2 col-xs-12 control-label">ค่าแรงรายวัน :</label>
							<div class="col-sm-3 col-xs-12">
								<input type="text" name="use_money" class="form-control" value="<?php echo $objqr_id['use_money']; ?>">
							</div>
							<label class="col-sm-1 control-label">บาท</label>
						</div>
				<div class="col-md-12" style="margin: 10px; text-align: center;">
						<button type="submit" class="btn btn-success">แก้ไขข้อมูล</button>
				</div>
				</form>
		</div>
	</div><!-- end row -->
</div> <!-- end containe

		
	</body>

</html>