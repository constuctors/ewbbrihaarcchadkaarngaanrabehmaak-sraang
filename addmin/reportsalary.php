<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	date_default_timezone_set("Asia/Bangkok");

	//echo $objresult["use_fname"];	
		
 ?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผู้ดูแลระบบ<?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<style>
	#h1:hover  {
		background-color: #DCDCDC;
	}
</style>
	</head>


	<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->
<div class="container" >
	<div class="row" >
			
		<div class="col-md-10 col-md-offset-1">
			<div class="col-md-2" style="float: left;">
				<a href="form_salary.php">
					<button  type="button" class="btn btn-danger">
						ย้อนกลับ
					</button>
				</a>
			</div>
		</div>
		<script>
			 $(function(){
			 	$('#btnmclick').on('click',function(evt){
					$.ajax({
						url: 'reportyear.php',
						type: "POST",
						data: $("#searchyear").serialize() ,
					 	success : function(result){
						$("#reportyear").html(result);
						}
					});
				});
			});
		</script>
		<div class="col-md-8 col-md-offset-2" style="background-color: #F5F5F5;  border-radius: 5px; padding: 10px;margin-top: 10px;">
			<div class="panel-heading">
				<h2 class="section-title" style="text-align: center;">รายงานการออกเงินเดือนพนักงาน</h2>
			</div>
			<form  class="form-horizontal" id="searchyear" method = "post" enctype="multipart/form-data">
				<div class="contact-form ">
					<div class="form-group ">
						<label class="col-sm-4 control-label">ปีที่ต้องการค้นหา :</label>
						<div class="col-sm-6">
							<input type="text" name="year_occ" class="form-control"  placeholder="ปี(2010)..">
						</div>
						<div class="col-md-2">
							<button  type="button" class="btn btn-primary" id="btnmclick">
								ค้นหา
							</button>
						</div>
					</div>
				</div>
			</form>
			<div class="col-md-12" id="reportyear">
				
			</div>
		</div>

	</div><!--end row-->
</div><!--end container-->
	</body>

</html>