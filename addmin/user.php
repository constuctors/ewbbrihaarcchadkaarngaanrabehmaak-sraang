<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='index.php';";
		echo "</script>";
		exit();
	}
	date_default_timezone_set("Asia/Bangkok");

	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายชื่อลูกค้า</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="../js/datatable/css/jquery.dataTables.css">
	<!-- jQuery -->
	<script type="text/javascript" src="../js/datatable/js/jquery-1.10.2.min.js"></script>
	 <!-- DataTables -->
	<script type="text/javascript" src="../js/datatable/js/jquery.dataTables.js"></script>
	
	<script>
		$(document).ready(function() {
			$('#table').DataTable();
		} );
	</script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: #3498DB;>
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-2">
			<div class="col-md-12">
				<a href="indexA.php">
					<button type="button" class="btn btn-danger">ย้อนกลับ</button>
				</a>
			</div>
		</div>
		<div class="col-md-12>"></div>
		<div class="col-md-8" style="background-color: #E0F2F7;">
			<h2 align="center">รายชื่อลูกค้า</h2>
			<table id="table" border="1" width="100%" class="contact-form  height = "60" class="body">
 		
		 		<thead align="center" style="background-color: #F5F5F5;text-align: center;">
			 		<b>
						<td height="40px" align="center" >รูป</td>
						<td height="40px" align="center" >ชื่อ-นามสกุล</td>
						<td height="40px" align="center" >เบอร์โทรศัพท์</td>
						<td align="center" >รายละเอียด	</td>
					</b>
				</thead>
			<?php 
				$selectuser =	"	select *
							from user,status
							where user.status_id = status.status_id
								and user.status_id = 2
						" ;
				$qruser = mysqli_query($condb,$selectuser);
				while ($aruse = mysqli_fetch_array($qruser)) 
				{
			 ?>
			 	<tr>
			 		<td>
			 			<img src="../picture/<?php echo $aruse['use_image']; ?>" alt="<?php echo $aruse['use_image']; ?>" width="100px" height="150px">
			 		</td>
			 		<td>
			 			<?php echo $aruse['use_fname']." ".$aruse['use_fname']; ?>
			 		</td>
			 		<td>
			 			<?php echo $aruse['use_phone']; ?>
			 		</td>
			 		<td align="center">
			 			<a href="userdetail.php?use_id=<?php echo $aruse['use_id']; ?>">
			 				<button  type="button" class="btn btn-success">รายละเอียด</button>
			 			</a>
			 		</td>
			 	</tr>
			<?php } ?>
			</table>

			<div class="col-md-12" style="height: 20px;">
			</div>
		</div>
		
		<div class="col-md-2"></div>
	</div><!-- close row-->
</div><!-- close container-->

</body>
</html>