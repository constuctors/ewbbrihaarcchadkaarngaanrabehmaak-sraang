<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	date_default_timezone_set("Asia/Bangkok");

	//echo $objresult["use_fname"];	
		
 ?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผู้ดูแลระบบ<?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<style>
	#h1:hover  {
		background-color: #DCDCDC;
	}
</style>
	</head>


	<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->
<div class="container" >
		<div class="row" >
			
			<div class="col-md-10 col-md-offset-1">
				<div class="col-md-2" style="float: left;">
					<a href="indexA.php">
						<button  type="button" class="btn btn-danger">
							ย้อนกลับ
						</button>
					</a>
				</div>
				<div class="col-md-8"></div>
				<div class="col-md-2">
					<a href="reportsalary.php">
						<button style="margin: 10px; ri" type="button" class="btn btn-info">
							ประวัติการออกเงินเดือน
						</button>
					</a>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6 col-md-8 col-md-offset-2">
				
			<form action="form-receipt.php" method="post">
				<div class="panel panel-info">
					<div class="panel-heading">
						<h2 class="section-title">สรุปเงินเดือนพนักงาน</h2>
					</div>
					<div class="panel-body" >
						<div class="col-xs-12 col-sm-6 col-md-12">
						<?php 
							$datenow =date('Y-m-d'); //echo $datenow;
							$days =  date('d'); 
							$month = date('m');
							$year = date('Y');
							//echo $days."/".$month."/".$year;

							$endmonth = date("t",strtotime($datenow));

							$selectuser = 	"	select *
											from user , status 
											where user.status_id = status.status_id and status_name =  'personal'
										";
							$qruser = mysqli_query($condb,$selectuser);

							while ($aruser = mysqli_fetch_array($qruser)) 
							{
								
								$selectsummoney = 	"	select 
														 (
																select count(job_date)as das 
																from jobsheet 
																where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	'$year-$month-1' 
																	and '$year-$month-$endmonth'
																	and job_status = 0
																	and job_check= 1
														) as numdate,

														us.use_money as wage,

														(
															select sum(occ_num) 
															from occupier as oc,user 
															where user.use_id = oc.use_id 
																and oc.use_id = ' ".$aruser['use_id']." '
																and occ_date between  
																'$year-$month-1' 
																and '$year-$month-$endmonth'
																and occ_status = 0
														) as sumocc , 

														(
															select count(job_date)as das 
															from jobsheet 
															where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	'$year-$month-1' 
																	and '$year-$month-$endmonth'
																	and job_status = 0
														)
														 as dates ,

														(
																select sum(job_bonus)
																from jobsheet 
																where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	'$year-$month-1' 
																	and '$year-$month-$endmonth'
																	and job_status = 0
														) as jobbonus

													from user as us , jobsheet
													where jobsheet.use_id = us.use_id  
														and  jobsheet.use_id = ' ".$aruser['use_id']." '    
														group by jobsheet.use_id 
												";
								$qrmoneyuser = mysqli_query($condb,$selectsummoney);
								$objmoney = mysqli_fetch_array($qrmoneyuser,MYSQLI_ASSOC);
								if ($objmoney['sumocc'] =="") 
								{
									$objmoney['sumocc'] = 0;
								}

								
								$sumwage = $objmoney['wage'] * $objmoney['numdate'];
								$totalwages = $sumwage + $objmoney['jobbonus'];
								$salary = $totalwages - $objmoney['sumocc'];
						 ?>
							<div class="col-xs-12 col-sm-6 col-md-12" id="h1" style="border: solid 1px #F5F5F5; border-radius: 5px; margin: 10px; background-color: #F5F5F5; 
							">
								<div class="row" style="padding: 10px 0 10px 0;">
									<div class="col-xs-12 col-sm-6 col-md-12">
										<img class="img-circle col-lg-3" style="margin: 10px 0 10px 0;" src="../picture/<?php echo $aruser['use_image'] ?>"   width="100" height="100">
										<div class="col-lg-9" style="padding: auto;">
											<p class="lead blog-description" style="color: #3366FF;">
												ชื่อ - นามสกุล: 
												<?php echo $aruser['use_fname']."&nbsp;".$aruser['use_lname']; ?>&nbsp; &nbsp; 
											</p>
											<h4 class="list-group-item-heading"  style="color: #FA8072;">
												ยอดเงินคงเหลือ : 
												<?php echo $salary; ?>&nbsp;  บาท 
											</h4>
											<h4 class="list-group-item-heading">
												จำนวนวันที่ทำงาน : <?php echo $objmoney['numdate']; ?>&nbsp;วัน
											  	&nbsp;
											  	ยอดเบิกเงินล่วงหน้า : <?php echo $objmoney['sumocc'] ; ?>&nbsp;บาท
											  	&nbsp;
											  	ค่าแรง/วัน : <?php echo $objmoney['wage'] ; ?>&nbsp;บาท&nbsp; 
												รวมค่าแรงพิเศษ : <?php echo $objmoney['jobbonus']; ?>&nbsp;  บาท
											  </h4>

										</div>
									</div>
								</div>
							</div>
							
						 <?php } ?>
						 </div>
					</div>
				</div>
				<?php if ($days == $endmonth) 
				{

				?>
				<div class="col-xs-12 col-sm-6 col-md-12" style="text-align: center;">
					<button type="submit" class="btn btn-success">ออกใบเสร็จรับเงิน</button>&nbsp;
					<a href="indexA.php"><button type="button" class="btn btn-danger">ยกเลิก</button></a>
				</div>	
				<?php } ?>
				</form>	
			</div>	

			<div class="col-xs-12 col-sm-6 col-md-2"></div>
	</div><!--end row-->
</div><!--end container-->
	</body>

</html>