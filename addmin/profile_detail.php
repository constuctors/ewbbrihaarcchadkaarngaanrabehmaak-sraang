<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	date_default_timezone_set("Asia/Bangkok");
	

	$pro_id = $_POST['pro_id'];
 ?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผลงานต่างๆ</title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script>
			$(function(){
				$('#txt_file').on('change',function(evt){
					// alert($(this).val());
					var mydata = new FormData();
					for (var i = 0; i <= $("#txt_file")[0].files.length ; i++) {
						mydata.append('txt_file[]',$("#txt_file")[0].files[i]);
					}
					$.ajax({
						url : "multiuploads_get.php",
						data : mydata,
						type : "POST",

						contentType : false ,
						processData : false ,
						beforeSend : function(){
							// $("tag").html('<i class="fa fa-spinner fa-pulse"></i>');
						},
						success : function(result){
							// $("tag").html(result);
							//alert(result);
							$("#divdisplay").html(result);
						}
						
					});
				});
			});
		</script>
	</head>


	<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->

<div class="container">

	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-2">
			<a href="profile_form.php">
				<button type="button" class="btn btn-danger">ย้อนกลับ</button>
			</a>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-8" style="background-color: #F5F5F5; padding: 10px;">
			<form action="profile_update.php" method="post">
			<center><h3>รายละเอียดผลงาน</h3></center>
			<div class="col-xs-12 col-sm-6 col-md-12 form-horizontal">
				<?php 
					$selproid = 	"	select *
										from profile , category
										where pro_id = ' ".$pro_id." ' and profile.pro_type = category.cate_id
									";
					$qrproid  = mysqli_query($condb,$selproid) or die(mysqli_error($condb));
					$objproid =mysqli_fetch_array($qrproid,MYSQLI_ASSOC);

				?>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:right;">
						ชื่องาน :
					</label>
					<div class="col-sm-9">
						<input type="hidden" name="pro_id" value="<?php echo $objproid['pro_id']; ?>">
						<input type="text" class="form-control" name="pro_name" value="<?php echo $objproid['pro_name']; ?>" >
					</div>
				</div>
				<div class="form-group">
					<div class="form-group">
						<label class="col-sm-3 control-label"  style="text-align:right;">
								ประเภทงาน :
						</label>
						<div class="col-sm-9">
							<?php 
								$selectcate = 	"	select *
											from category
										";
								$qrcate = mysqli_query($condb,$selectcate);
								while ($arrcate = mysqli_fetch_array($qrcate)) 
								{	
							?>
							<label  class="checkbox-inline">
								<input type="radio" name="pro_type"  value="<?php echo $arrcate['cate_id']; ?>" 
								<?php 
										if ($objproid['pro_type'] == $arrcate['cate_id']) 
										{
											echo "checked";
										}
								 ?>> 
								<?php echo $arrcate['cate_name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							</label>	
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:right;">
						วันที่ลง :
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" name="pro_date" value="<?php echo $objproid['pro_date']; ?>" readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:right;">
						สถานที่ :
					</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="pro_address" rows="3"><?php echo $objproid['pro_address']; ?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:right;">
						รายละเอียด :
					</label>
					<div class="col-sm-9">
						<textarea class="form-control" name="pro_description" rows="3" ><?php echo $objproid['pro_description']; ?>
						</textarea>
					</div>
				</div>
				<div class="form-group">
				<?php
					$selimg = 	"	select ppic_name
								from propicture as pp , profile
								where 	pp.pro_id = profile.pro_id 
									and   pp.pro_id = $pro_id
							"; 
					$qrimg = mysqli_query($condb,$selimg) or die(mysqli_error($condb));
					while ($arrimg = mysqli_fetch_array($qrimg)) 
					{
						$i = $i+1;
				?>
					
					<div class="col-sm-6 col-md-4" id="target<?php echo $valuei; ?>">
						<div class="thumbnail">
							<img src="../picture/profile/<?php echo $arrimg['ppic_name']; ?>"  alt="" >
							<div class="caption">
								<center>
									<?php echo $arrimg['ppic_name']; ?>
								</center>
							</div>
							<input type="button" class="btn btn-danger" height="300px" id="btn<?php echo $i; ?>"  value = "ลบรูปภาพ">
						</div>
					</div>
					<script>
						$(document).ready(function(){  
							$( "#btn<?php echo $i; ?>" ).click(function()
							{
								
								$.ajax({
									 type: "GET",
									url : "deleteimg.php?mydata=<?php echo $arrimg['ppic_name']; ?>",
									success : function(result){
										// $("tag").html(result);
									}
								});				
							});
						});
					</script>
				<?php } ?>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" style="text-align:right;">
						เพิ่มรูป :
					</label>
					<div class="col-xs-12 col-sm-6 col-md-9">
						<input type="file" id="txt_file" name="txt_file" multiple  accept="image/*" />
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12 col-xs-4" id="divdisplay">
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-12">
					<CENTER>
						<button type="submit" class="btn btn-info">
								แก้ไขข้อมูล
						</button>
			</form>
						<form action="profile_delete.php" method="post">
							<input type="hidden" name="pro_id" value="<?php echo $objproid['pro_id']; ?>">
							<button type="submit" class="btn btn-danger">
								ลบข้อมูล
							</button>		
						</form>
					</CENTER>
				</div>
			</div>
		</div>

		<div class="col-xs-12 col-sm-6 col-md-2"></div>
	</div>
</div>
</body>
</html>