<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='../index.php';";
		echo "</script>";
		exit();
	}
	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	date_default_timezone_set("Asia/Bangkok");
	$year_occ = $_POST['year_occ'];

	$seletyear = 	"	select  	year(job_date) as year_occ
				from 	jobsheet
				where 	year(job_date) = '$year_occ'
				group by  year(job_date)
			";
	$qryear = mysqli_query($condb,$seletyear);
	$objyear = mysqli_fetch_array($qryear , MYSQLI_ASSOC);

	$seletmonth = 	"	select 	month(job_date) as month_occ
					from 	jobsheet
					where 	month(job_date) between '1' and '12'
						and year(job_date) = ' ".$objyear['year_occ']." '
					group by month(job_date)
				";
	$qrmonth = mysqli_query($condb,$seletmonth);
	while ( $arrmonth = mysqli_fetch_array($qrmonth)) 
	{
				$num_month = $arrmonth['month_occ'];
				$strMonthCut = Array("" ,
									"มกราคม" , "กุมภาพันธ์" , "มีนาคม" 
									, "เมษายน" , "พฤษภาคม" , "มิถุนายน" 
									, "กรกฎาคม" , "สิงหาคม" , "กันยายน" 
									, "กันยายน" , "พฤศจิกายน" , "ธันวาคม"
							);
				$num_month = $strMonthCut[$num_month];

?>
					
				<div class="panel panel-primary">
						<div class="panel-heading" id="show<?php echo $arrmonth['month_occ'] ?>">
							<h3 class="panel-title">เดือน : <?php echo $num_month ?></h3>
						</div>
						<div class="panel-body">

						
						<div class="col-xs-12 col-sm-6 col-md-12" id="<?php echo $arrmonth['month_occ'] ?>" style="display: none;">
						
						<?php 
							$endmonth = date("t",strtotime($datenow));

							$selectuser = 	"	select *
											from user , status 
											where user.status_id = status.status_id and status_name =  'personal'
										";
							$qruser = mysqli_query($condb,$selectuser);

							while ($aruser = mysqli_fetch_array($qruser)) 
							{
								
								$selectsummoney = 	"	select 
														 (
																select count(job_date)as das 
																from jobsheet 
																where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-1 ' 
																	and ' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-$endmonth'
																	and job_status = 0
																	and job_check= 1
														) as numdate,

														us.use_money as wage,

														(
															select sum(occ_num) 
															from occupier as oc,user 
															where user.use_id = oc.use_id 
																and oc.use_id = ' ".$aruser['use_id']." '
																and occ_date between  
																' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-1' 
																and '".$objyear['year_occ']."-".$arrmonth['month_occ']."-$endmonth'
																and occ_status = 0
														) as sumocc , 

														(
															select count(job_date)as das 
															from jobsheet 
															where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-1' 
																	and ' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-$endmonth'
																	and job_status = 0
														)
														 as dates ,

														(
																select sum(job_bonus)
																from jobsheet 
																where jobsheet.use_id = ' ".$aruser['use_id']." ' 
																and  job_date between  
																	' ".$objyear['year_occ']."-".$arrmonth['month_occ']."-1' 
																	and '".$objyear['year_occ']."-".$arrmonth['month_occ']."-$endmonth'
																	and job_status = 0
														) as jobbonus

													from user as us , jobsheet
													where jobsheet.use_id = us.use_id  
														and  jobsheet.use_id = ' ".$aruser['use_id']." '    
														group by jobsheet.use_id 
												";
								$qrmoneyuser = mysqli_query($condb,$selectsummoney) or die(mysqli_error($condb));
								$objmoney = mysqli_fetch_array($qrmoneyuser,MYSQLI_ASSOC);
								if ($objmoney['sumocc'] =="") 
								{
									$objmoney['sumocc'] = 0;
								}
								$sumwage = $objmoney['wage'] * $objmoney['numdate'];
								$totalwages = $sumwage + $objmoney['jobbonus'];
								$salary = $totalwages - $objmoney['sumocc'];
						 ?>
							<div class="col-xs-12 col-sm-6 col-md-12" id="h1" style="border: solid 1px #F5F5F5; border-radius: 5px; margin: 10px; background-color: #F5F5F5; 
							">
								<div class="row" style="padding: 10px 0 10px 0;">
									<div class="col-xs-12 col-sm-6 col-md-12">
										<img class="img-circle col-lg-3" style="margin: 10px 0 10px 0;" src="../picture/<?php echo $aruser['use_image'] ?>"   width="100" height="100">
										<div class="col-lg-9" style="padding: auto;">
											<p class="lead blog-description" style="color: #3366FF;">
												ชื่อ - นามสกุล: 
												<?php echo $aruser['use_fname']."&nbsp;".$aruser['use_lname']; ?>&nbsp; &nbsp; 
											</p>
											<h4 class="list-group-item-heading"  style="color: #FA8072;">
												ยอดเงินคงเหลือ : 
												<?php echo $salary; ?>&nbsp;  บาท 
											</h4>
											<h4 class="list-group-item-heading">
												จำนวนวันที่ทำงาน : <?php echo $objmoney['numdate']; ?>&nbsp;วัน
											  	&nbsp;
											  	ยอดเบิกเงินล่วงหน้า : <?php echo $objmoney['sumocc'] ; ?>&nbsp;บาท
											  	&nbsp;
											  	ค่าแรง/วัน : <?php echo $objmoney['wage'] ; ?>&nbsp;บาท&nbsp; 
												รวมค่าแรงพิเศษ : <?php echo $objmoney['jobbonus']; ?>&nbsp;  บาท
											  </h4>

										</div>
									</div>
								</div>
							</div>
							
						 <?php } ?>
						 </div>
					</div>
					<script>
						$(document).ready(function(){
							$("#show<?php echo $arrmonth['month_occ'] ?>").click(function()
							{
								$("#<?php echo $arrmonth['month_occ'] ?>").toggle(1000);

							});		
						});
					</script>
				</div>
<?php 
	
	} 
?>
		

