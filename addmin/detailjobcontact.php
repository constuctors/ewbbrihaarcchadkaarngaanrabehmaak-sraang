<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "addmin" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ ผู้ดูแลระบบกรุณาออกไปครับ');window.location='index.php';";
		echo "</script>";
		exit();
	}
	date_default_timezone_set("Asia/Bangkok");

	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$_SESSION['con_id'] = $_POST['con_id'];

	$con_id = $_SESSION['con_id'];
	$qr = "		select *
			from 	user , jobcontact as jc , constatus as cn 
			where	jc.con_status = cn.const_id 
				and user.use_id = jc.use_id 
				and jc.con_id = ' ".$con_id. " ' 
		";
	$show = mysqli_query($condb,$qr);
	$detail = mysqli_fetch_array($show,MYSQLI_ASSOC); 	
		
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>รายละเอียดของงาน</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	
	<script>

		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').focus()
		});
		$('#mysendjob').on('shown.bs.modal', function () {
			$('#myInput').focus()
		});	

		function checkinput ()
		{

		}
	</script>
</head>
<body>
<nav class="navbar navbar-default" style="background-color: #3498DB;>
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="../addmin/indexA.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<div class="container">
	<div class="row">

	<?php  if ($detail['const_id'] == 0 ) {?>
		<div class="col-md-12">
			<a href="showuser.php">
				<button type="button" class="btn btn-danger">ย้อนกลับ</button>
			</a>
		</div>
	<?php } ?>
	<?php  if ($detail['const_id'] == 1 ) {?>
		<div class="col-md-12">
			<a href="show_contact.php">
				<button type="button" class="btn btn-danger">ย้อนกลับ</button>
			</a>
		</div>
	<?php } ?>
	<?php  if ($detail['const_id'] == 2 ) {?>
		<div class="col-md-12">
			<a href="show_notcontact.php">
				<button type="button" class="btn btn-danger">ย้อนกลับ</button>
			</a>
		</div>
	<?php } ?>
	<?php  if ($detail['const_id'] == 4 ) {?>
		<div class="col-md-12">
			<a href="indexA.php">
				<button type="button" class="btn btn-danger">ย้อนกลับ</button>
			</a>
		</div>
	<?php } ?>
		<div class="col-md-2"></div>
		<div class="col-md-8">
		
			<h3>รายละเอียดของงาน</h3>
			<br>
			<div class="row">	
				<div class="col-md-12">
					วันที่ติดต่องาน : <?php echo $detail['con_datetime']; ?>
				</div>
			</div>
			<br>
			<form action="detailjob_update.php" method="post" id="fromnewsum">
				<table class="table table-bordered table-striped" border="0px">
						<input type="hidden" name="con_id" value="<?php echo  $detail['con_id'] ;?>">
						<tr>
							<td width="150px">
								<b>ชื่อผู้ติดต่องาน :</b>
							</td>
							<td>
								<?php 
									echo $detail['use_fname']."&nbsp&nbsp&nbsp".$detail['use_lname']; 
								?>
							</td>
						</tr>

						<tr>
							<td width="150px">
								<b>สถานที่งาน :</b>.
							</td>
							<td>
								<?php echo $detail['con_address']; ?>
							</td>
						</tr>

						<tr>
							<td width="150px">
								<b>ประเภทงาน :</b>
							</td>

							<td>
								<table>
									<tr>
										<td>
											<?php 
												$selectcate = " 	select cd.cate_id , cate_name
															from 	contactdetail as cd , 
																category as cate
															where	cd.cate_id = cate.cate_id
																and con_id = ' ".$detail['con_id']." '
															group by cd.cate_id
														 ";
												$qrcate = mysqli_query($condb,$selectcate);
												while ($arcate = mysqli_fetch_array($qrcate)) 
												{
													echo " ' ".$arcate['cate_name']." ' ";
												}
											 ?>
										</td>
									</tr>
								</table>
							</td>

						</tr>
						<tr>
							<td width="150px">
								<h4>ลักษณะงาน :</h4> 
							</td>
							<td>
								<table width="100%">
								<?php 
									$selectcd = " 		select 	cd.get_scale as scale , get_name , get_money
												from 	contactdetail as cd , 
													category as cate , 
													getjob
												where	cd.cate_id = cate.cate_id
													and cd.get_id = getjob.get_id
													and con_id = ' ".$detail['con_id']." '
											 ";
									$qrjd = mysqli_query($condb,$selectcd);
									while ($arjd = mysqli_fetch_array($qrjd)) 
									{
								 ?>
									<tr>
										<td style="width: 30%;">
											<?php echo $arjd['get_name']; ?> :
										</td>
										<td style="width: 40%;">
											<?php echo $arjd['scale']; ?>  ตารางเมตร
										</td>
										<td style="width: 10%;">
											ราคา :
										</td>
										<td style="width: 20%;">
											<?php echo  $arjd['scale'] * $arjd['get_money']; ?>  บาท
											
										</td>
									</tr>
								<?php } ?>
								</table>

							</td>
						</tr>
						<tr>
							<td>
								<b>สรุปราคางาน :</b>
							</td>
							<td style="text-align: center; color: red;" >
								<b><?php echo $detail['con_totalprice']; ?>  บาท</b>
							</td>
						</tr>
					</table>
					<?php 
						if ($detail['con_status'] == 0) {
					 ?>
					<div class="col-md-12" style="text-align: center;">
						<input type="radio" name="con_status" value="1"> &nbsp;ตอบรับการจ้างงาน &nbsp;
						<input type="radio" name="con_status" value="2"> &nbsp;ปฏิเสธการจ้างงาน
					</div>	
					<div class="col-md-12" style="text-align: center;">
						<button type="submit" class="btn btn-success">ยืนยัน</button>
					</div>	
					<?php } ?>
				</form>
<!--============================================close detailjob============================================== -->
		<?php 
			if ($detail['con_status'] == 1) {
		 ?>
			<div class="col-md-12" style="text-align: center;">
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">ส่งงานลูกค้า</button>
				</div>
		
		</div>
<!--============================================modal sendjob============================================== -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
						 </button>
						<h4 class="modal-title" id="myModalLabel">ส่งงานลูกค้า</h4>
					</div>

				<div class="modal-body">
					<form action="occupierjob_insert.php" method="post" >

						<input type="hidden"  name="ocj_datetime"  id="con_datetime" value="<?=date('Y-m-d H:i:s')?>"/>
	 					<input type="hidden" name="con_id" value="<?php echo $con_id; ?>">

						<table class="table table-bordered table-striped" border="0px">
							<?php 
								$selectcds = " 	select 	cd.get_id , get_name, cd.get_scale as scl  , 
												get_money
											from 	contactdetail as cd , 
												category as cate , 
												getjob
											where	cd.cate_id = cate.cate_id
												and cd.get_id = getjob.get_id
												and cd.conde_status = 0
												and con_id = ' ".$detail['con_id']." '
										";
								$qrjds = mysqli_query($condb,$selectcds);
								while ($arjds = mysqli_fetch_array($qrjds)) 
								{
										
							 ?>
							 <tr>
							 	<td >
							 		<?php echo $arjds['get_name']; ?> :
									<input type="checkbox"  name="get_id[]"  id="sendjob<?php echo $arjds['get_id']; ?> " value="<?php echo $arjds['get_id']; ?>">
							 	</td>
							 	<td >
							 		<div class="form-group">
							 			<div class="col-xs-2">	
											<label for="exampleInputPassword1">ราคา</label>
							 			</div>

										<div class="col-xs-8">
											<input type="text"   readonly id="ocj_num<?php echo $arjds['get_id']; ?> " class="form-control input-sm" value="<?php echo  $arjds['scl'] * $arjds['get_money']; ?> ">
										</div>

										<div class="col-xs-2">
											<label for="exampleInputPassword1">บาท</label>
							 			</div>
										
									</div>

							 	</td>
							 </tr>
							 <?php 
								} 
							 ?>
							 <?php 
								$checks =	"	select sum(ocj_num) as sumnum
											from occupier_job
											where con_id = '$con_id'
								   		";
								$qrs = mysqli_query($condb,$checks);
								$sunocc = mysqli_fetch_array($qrs,MYSQLI_ASSOC);
											
								if ($sunocc['sumnum'] == "") 
								{
									$sunocc['sumnum']  = 0;				
								}
								$sunocc['sumnum'] = $detail['con_totalprice'] - $sunocc['sumnum'];

								if ($sunocc['sumnum'] != 0 ) 
								{
							 ?>

							 <tr>
							 	<td>
							 		<label for="exampleInputName2">เบิกเงิน</label>
							 	</td>
							 	<td>
								 	<div class="form-group">
								 		<div class="col-xs-8">
											<input type="text" name="ocj_num" class="form-control col-md-7" placeholder="บาท....">
										</div>
										
										<label for="exampleInputName2">บาท</label>

									
										
										 <div class="col-xs-12" style="font-size: 12px; color: red; margin-top: 10px;">
											<label for="exampleInputName2">ยอดที่สามารถเบิกได้  :</label>
											<b><?php echo $sunocc['sumnum']; ?></b>
										</div>
										
									</div>
							 	</td>
							 </tr>
						</table>
						<?php 
								}else
								{
							 ?>
							 <div class="col-sm-6 col-md-5 col-lg-12">
							 	<input type="radio" name="con_status" value="4"> &nbsp;ปิดไซต์งาน &nbsp;
							 </div>
							 <?php } ?>

						<div class="row">
							
							<div class="col-sm-6 col-md-5 col-lg-2">
								
							</div>
							<div class="col-sm-12 col-md-12 col-lg-12">

								<h4>รหัสผ่านลูกค้า</h4>

								<div class="form-group">
									<label for="exampleInputEmail1">Username</label>
									<input type="text" class="form-control input-sm" name="username" id="exampleInputEmail1" placeholder="Username....">
								</div>
								<div class="form-group">
									<label for="exampleInputPassword1">Password</label>
									<input type="password" name="password" class="form-control input-sm" id="exampleInputPassword1" placeholder="Password....">
								</div>
							</div>
						</div><!-- end row-->
				</div>
				<div class="modal-footer" style=" text-align: center;">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save changes</button>
				</div>
				</form>
			</div>

		<?php } ?>
		</div>
<!--===========================================/modal sendjob============================================== -->



		<div class="col-md-2"></div>
	</div><!-- close row-->
</div><!-- close container-->

</body>
</html>