<?php 
	ob_start();
	session_start();


	if ($_SESSION['use_id'] != "")
	{
		if ($_SESSION["status_name"] != "user" )
		{
			echo "<script language=\"JavaScript\">";
			echo "alert('คูณไม่ใช่ ลูกค้ากรุณาออกไปครับ');window.location='index.php';";
			echo "</script>";
		}
		require 'connect/connecDb.php';
		$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
		$result = mysqli_query($condb,$query);
		$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

		$con_id = $_GET['id'];
		$qr = "		select *
			from 	user , jobcontact as jc , constatus as cn 
			where	jc.con_status = cn.const_id 
				and user.use_id = jc.use_id 
				and jc.con_id = ' ".$con_id. " ' 
			";
		$show = mysqli_query($condb,$qr);
		$detail = mysqli_fetch_array($show,MYSQLI_ASSOC); 
	}		

 ?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ข้อมูลส่่วนตัว</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
    
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
	<div id="site-content">
		<header class="site-header">
			<div class="top-header">
				<div class="container">
					<a href="tel:80049123441">Call Us: 086-478-1761</a>
					<nav class="member-navigation pull-right">
						<?php  if($_SESSION['use_id'] != "")
							{
						?>
							<a href="detailuser.php"><i class="fa fa-user"></i>
								<?php echo $objresult['use_fname'];echo "&nbsp";echo $objresult['use_lname'];?>
							</a>
							<a href="logout.php"><i class="fa fa-lock"></i> ออกจากระบบ</a>
						<?php 
							}else{ 
						?>
						<a href="fromLogin.php"><i class="fa fa-user"></i> Register</a>	
						<a href="fromLogin.php"><i class="fa fa-lock"></i> Login</a>
						<?php 	} ?>
					</nav> <!-- .member-navigation -->
				</div> <!-- .container -->
			</div> <!-- .top-header -->
			<div class="bottom-header">
				<div class="container">
					<a href="index.php" class="branding pull-left">
						<img src="images/logo-icon.png" alt="Site title" class="logo-icon">
						<h1 class="site-title">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></h1> 
						<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
					</a> <!-- #branding -->
						
					<nav class="main-navigation pull-right">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="profilenews.php">ผลงาน</a></li>
							<li class="menu-item"><a href="showjob.php">ตรวจสอบราคางาน</a></li>
							<li class="menu-item"><a href="contact.php">ติดต่องาน</a></li>
						</ul>
					</nav> <!-- .main-navigation -->
				</div> <!-- .container -->
			</div> <!-- .bottom-header -->
			</header> <!-- .site-header -->

			<main class="content">

				<div class="container">
					<div class="rows">
		<div class="col-md-8 col-md-offset-2">
		
			<h3>รายละเอียดของงาน</h3>
			<br>
			<div class="row">	
				<div class="col-md-12">
					วันที่ติดต่องาน : <?php echo $detail['con_datetime']; ?>
				</div>
			</div>
			<br>
			<form action="detailjob_update.php" method="post" id="fromnewsum">
				<table class="table table-bordered table-striped" border="0px">
						<input type="hidden" name="con_id" value="<?php echo  $detail['con_id'] ;?>">
						<tr>
							<td width="150px">
								<b>ชื่อผู้ติดต่องาน :</b>
							</td>
							<td>
								<?php 
									echo $detail['use_fname']."&nbsp&nbsp&nbsp".$detail['use_lname']; 
								?>
							</td>
						</tr>

						<tr>
							<td width="150px">
								<b>สถานที่งาน :</b>.
							</td>
							<td>
								<?php echo $detail['con_address']; ?>
							</td>
						</tr>

						<tr>
							<td width="150px">
								<b>ประเภทงาน :</b>
							</td>

							<td>
								<table>
									<tr>
										<td>
											<?php 
												$selectcate = " 	select cd.cate_id , cate_name
															from 	contactdetail as cd , 
																category as cate
															where	cd.cate_id = cate.cate_id
																and con_id = ' ".$detail['con_id']." '
															group by cd.cate_id
														 ";
												$qrcate = mysqli_query($condb,$selectcate);
												while ($arcate = mysqli_fetch_array($qrcate)) 
												{
													echo " ' ".$arcate['cate_name']." ' ";
												}
											 ?>
										</td>
									</tr>
								</table>
							</td>

						</tr>
						
						<tr>
							<td width="150px">
								<h4>ลักษณะงาน :</h4> 
							</td>
							<td>
								<table width="100%">
								<?php 
									$selectcd = " 		select 	cd.get_scale as scale , get_name , get_money
												from 	contactdetail as cd , 
													category as cate , 
													getjob
												where	cd.cate_id = cate.cate_id
													and cd.get_id = getjob.get_id
													and con_id = ' ".$detail['con_id']." '
											 ";
									$qrjd = mysqli_query($condb,$selectcd);
									while ($arjd = mysqli_fetch_array($qrjd)) 
									{
								 ?>
									<tr>
										<td style="width: 30%;">
											<?php echo $arjd['get_name']; ?> :
										</td>
										<td style="width: 40%;">
											<?php echo $arjd['scale']; ?>  ตารางเมตร
										</td>
										<td style="width: 10%;">
											ราคา :
										</td>
										<td style="width: 20%;">
											<?php echo  $arjd['scale'] * $arjd['get_money']; ?>  บาท
											
										</td>
									</tr>
								<?php } ?>
								</table>

							</td>
						</tr>
						
						<tr>
							<td width="150px">
								<b>รายงานการส่งงาน :</b>
							</td>

							<td>
								<table width="100%">
									<?php 
										$selectsend = " 	select 	*
													from 	contactdetail as cd 
														, sendjob as sj 
														, getjob as gj
													where cd.con_id = sj.con_id
														and cd.get_id = gj.get_id
														and cd.get_id = sj.con_id
														and cd.con_id = ' ".$con_id." '
														and conde_status = 1
												";
										$qrsend = mysqli_query($condb,$selectsend);
										while ($arrsend = mysqli_fetch_array($qrsend)) 
										{
									 ?>
									<tr align="center">
										<td>
											ชื่องาน : 
										</td>
										<td>
											<?php echo $arrsend['get_name'] ; ?>
										</td>
										<td>
											วันที่ส่ง : 
										</td>
										<td>
											<?php echo $arrsend['sen_datetime'] ; ?>
										</td>
									</tr>
									<?php } ?>
								</table>
							</td>

						</tr>

						<tr>
							<td>
								<b>ราคางาน :</b>
							</td>
							<td style="text-align: center; color: red;" >
								<b><?php echo $detail['con_totalprice']; ?>  บาท</b>
							</td>
						</tr>
						<tr>
							<td>
								<b>รายละเอียดการเบิกเงิน :</b>
							</td>
							<td>
								<table width="100%" class="table table-bordered table-striped">
									<thead >
										<td align="center">
											วันที่เบิก
										</td>
										<td align="center">
											จำนวนเงาน
										</td>
									</thead>
										<?php 
											$selectocc = 	"	select 	*
														from 	occupier_job as occj
														where occj.con_id = ' ".$con_id." '
													";
											$qrocc = mysqli_query($condb,$selectocc);
											while ( $arrocc = mysqli_fetch_array($qrocc)) 
											{
												$sum = $sum + $arrocc['ocj_num'];
										 ?>
									<tr align="center">
										<td>
											<?php echo $arrocc['ocj_datetime']; ?>
										</td>
										<td>
											<?php echo $arrocc['ocj_num']; ?> บาท
										</td>
									</tr>
									<?php } ?>
									<tr align="center">
										<td>
											<b>รวม</b>
										</td>
										<td>
											<b><?php echo $sum; ?></b> บาท
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</form>
				<div class="col-md-12" style="margin-bottom: 10px;">
					<center>
						<a href="detailuser.php">
							<button type="button" class="btn btn-danger">ย้อนกลับ</button>
						</a>
					</center>
					
				</div>
			</div>

<!--============================================close detailjob============================================== -->
		
				

 					
			</main> <!-- .content -->
			<footer class="site-footer wow fadeInUp">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							
							<div class=" branding">
								<img src="images/logo-footer.png" alt="Site title" class="logo-icon">
								<h1 class="site-title"><a href="#">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></a></h1> 
								<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
							</div> <!-- .branding -->

							<p class="copy">Copyright 2014 บริษัทช่างจ๊อดรับเหมาก่อสร้าง. designed by Themezy. All rights reserved</p>
						</div>
						
						<div class="col-md-6 align-right">
						
							<nav class="footer-navigation">
								<a href="profilenews.php">ผลงาน</a>
								<a href="showjob.php">ตรวจสอบราคางาน</a>
								<a href="contact.php">ติดต่องาน</a>
							</nav> <!-- .footer-navigation -->
							<!--
							<div class="social-links">
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
							</div>  .social-links -->
						
						</div>
					</div>

				</div>
			</footer> <!-- .site-footer -->
			

		</div> <!-- #site-content -->
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>
</html>