<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<body>
<form id="myform"  method="POST" class="form-horizontal">
	<input type="file" id="txt_file" name="txt_file" multiple value="" class="form-control" accept="image/*" />
</form>
<div id="divdisplay" class="some_class"></div>
<script>
$(function(){
	$('#txt_file').on('change',function(evt){
		// alert($(this).val());
		var mydata = new FormData();
		for (var i = 0; i <= $("#txt_file")[0].files.length ; i++) {
			mydata.append('txt_file[]',$("#txt_file")[0].files[i]);
		}
		$.ajax({
			url : "myuploads_get.php",
			data : mydata,
			type : "POST",

			contentType : false ,
			processData : false ,
			beforeSend : function(){
				// $("tag").html('<i class="fa fa-spinner fa-pulse"></i>');
			},
			success : function(result){
				// $("tag").html(result);
				//alert(result);
				$("#divdisplay").html(result);
			}
			
		});
	});
});
</script>
</body>
</html>