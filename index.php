<?php 
	ob_start();
	session_start();
?>
<?php 
	require './connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ช่างจ๊อดรับเหมาก่อสร้าง</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
		
		<div id="site-content">
			
			<header class="site-header">
				<div class="top-header">
					<div class="container">
						<a href="tel:80049123441">Call Us: 086-478-1761</a>
						
						<nav class="member-navigation pull-right">
							<?php  if($_SESSION['use_id'] != "")
							{
							?>
								<a href="detailuser.php"><i class="fa fa-user"></i>
									<?php echo $objresult['use_fname'];echo "&nbsp";echo $objresult['use_lname'];?>
								</a>
								<a href="logout.php"><i class="fa fa-lock"></i> ออกจากระบบ</a>
							<?php }else{ ?>
							<a href="fromLogin.php"><i class="fa fa-user"></i> Register</a>	
							<a href="fromLogin.php"><i class="fa fa-lock"></i> Login</a>
							<?php } ?>
						</nav> <!-- .member-navigation -->
					</div> <!-- .container -->
				</div> <!-- .top-header -->

				<div class="bottom-header">
					<div class="container">
						<a href="index.php" class="branding pull-left">
							<img src="images/logo-icon.png" alt="Site title" class="logo-icon">
							<h1 class="site-title">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></h1> 
							<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
						</a> <!-- #branding -->
						
						<nav class="main-navigation pull-right">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">
								<li class="menu-item"><a href="profilenews.php">ผลงาน</a></li>
								<li class="menu-item"><a href="showjob.php">ตรวจสอบราคางาน</a></li>
								<li class="menu-item"><a href="contact.php">ติดต่องาน</a></li>
							</ul>
						</nav> <!-- .main-navigation -->
					</div> <!-- .container -->
				</div> <!-- .bottom-header -->
			</header> <!-- .site-header -->

			<main class="content">
				<div class="slider">
					<ul class="slides">
						<li>
							<div class="container">
								<img src="picture/profile/dsc03540.jpg" width="300px" height="500px" alt="">
								<div class="slide-caption">
									<h2 class="slide-title">Duis aute reprehenderit</h2>
									<small class="slide-subtitle">Nemo enom ipsam voluptatem voluptas</small>
									<div class="slide-summary">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, illum laborum odit est quibusdam, molestias quaerat qui, eveniet voluptate debitis, earum dolorem. Fuga maxime fugit excepturi, saepe fugiat quisquam quia!</p>
									</div>
									<a href="" class="button">Read More</a>
								</div>
							</div>
						</li>
					</ul> <!-- .slides -->
				</div> <!-- .slider -->

				<div class="fullwidth-block feature-section">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<div class="feature wow fadeInUp">
									<div class="feature-title">
										<i class="icon-customer-service"></i>
										<h2 class="title">Customer Services</h2>
										<small class="subtitle">Nulla eros odio dolor</small>
									</div>
									<div class="feature-summary">
										<p>Chocolate caramels unerdwear.com lemon drops. Powder chupa chups pastry macaroon wafer chocolate cake sweet roll croissant jelly</p>
									</div>
									<a href="#" class="button">More info</a>
								</div> <!-- .feature -->
							</div> <!-- .col-md-4 -->
							<div class="col-md-4">
								<div class="feature wow fadeInUp" data-wow-delay=".2s">
									<div class="feature-title">
										<i class="icon-server-lock"></i>
										<h2 class="title">Customer Services</h2>
										<small class="subtitle">Nulla eros odio dolor</small>
									</div>
									<div class="feature-summary">
										<p>Chocolate caramels unerdwear.com lemon drops. Powder chupa chups pastry macaroon wafer chocolate cake sweet roll croissant jelly</p>
									</div>
									<a href="#" class="button">More info</a>
								</div> <!-- .feature -->
							</div> <!-- .col-md-4 -->
							<div class="col-md-4">
								<div class="feature wow fadeInUp" data-wow-delay=".4s">
									<div class="feature-title">
										<i class="icon-bar-chart-up"></i>
										<h2 class="title">Customer Services</h2>
										<small class="subtitle">Nulla eros odio dolor</small>
									</div>
									<div class="feature-summary">
										<p>Chocolate caramels unerdwear.com lemon drops. Powder chupa chups pastry macaroon wafer chocolate cake sweet roll croissant jelly</p>
									</div>
									<a href="#" class="button">More info</a>
								</div> <!-- .feature -->
							</div> <!-- .col-md-4 -->
						</div> <!-- .row -->
					</div> <!-- .container -->
				</div> <!-- .feature-section -->

				<div class="fullwidth-block about-section">

					<div class="container">

						<div class="row">

							<div class="col-md-6 wow fadeInUp">
								<h2>Safe &amp; high-speed hosting</h2>
								<p class="leading">Pallentesque nibh pharetra urna elementum viverra elit duis faucibus augue tempor eleifend</p>
								<p>Tiramisu cotton candy caramels cake biscuit jelly-o chupa chups chocolate. Tootsie roll lollipop topping. Macaroon ice cream cookie powder dessert gingerbread oat cake. Pudding cake powder icing tart sugar plum sesame snaps.</p>
								<p>Fruitcake tootsie roll candy. Sweet roll toffee donut. Chocolate cake gummi bears fruitcake cookie biscuit cotton candy marshmallow.</p>
								<p>Liquorice macaroon marshmallow macaroon cheesecake sweet soufflé. Cheesecake cookie dessert jelly-o. Fruitcake tart topping.</p>

							</div>

							<div class="col-md-6">
								<h2 class="wow fadeInRight">What can you expect?</h2>
								<hr class="separator">
								<ul class="feature-list">
									<li class="wow fadeInRight">
										<i class="icon-money-globe"></i>
										<h3>Aliquam nibh quam iaculis tempis</h3>
										<p>Candy powder. Carrot cake ice cream toffee bonbon. Donut marzipan chupa chups cookie tart dessert fruitcake brownie. </p>
									</li>
									<li class="wow fadeInRight">
										<i class="icon-server-lock"></i>
										<h3>Aliquam nibh quam iaculis tempis</h3>
										<p>Candy powder. Carrot cake ice cream toffee bonbon. Donut marzipan chupa chups cookie tart dessert fruitcake brownie. </p>
									</li>
									<li class="wow fadeInRight">
										<i class="icon-person-time"></i>
										<h3>Aliquam nibh quam iaculis tempis</h3>
										<p>Candy powder. Carrot cake ice cream toffee bonbon. Donut marzipan chupa chups cookie tart dessert fruitcake brownie. </p>
									</li>
								</ul>
							</div>

						</div> <!-- .row -->

					</div> <!-- .container -->
				</div> <!-- .fullwidth-block -->
			</main> <!-- .content -->

			<footer class="site-footer wow fadeInUp">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							
							<div class=" branding">
								<img src="images/logo-footer.png" alt="Site title" class="logo-icon">
								<h1 class="site-title"><a href="#">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></a></h1> 
								<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
							</div> <!-- .branding -->

							<p class="copy">Copyright 2014 บริษัทช่างจ๊อดรับเหมาก่อสร้าง. designed by Themezy. All rights reserved</p>
						</div>
						
						<div class="col-md-6 align-right">
						
							<nav class="footer-navigation">
								<a href="profilenews.php">ผลงาน</a>
								<a href="showjob.php">ตรวจสอบราคางาน</a>
								<a href="contact.php">ติดต่องาน</a>
							</nav> <!-- .footer-navigation -->
							<!--
							<div class="social-links">
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
							</div>  .social-links -->
						
						</div>
					</div>

				</div>
			</footer> <!-- .site-footer -->

		</div> <!-- #site-content -->

		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>