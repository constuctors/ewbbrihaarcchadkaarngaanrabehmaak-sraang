<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "personal" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ พนักงานกรุณาออกไปครับ!');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>พนักงาน : <?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>

<style>
	.headerT{
		border: solid 1px white ;
		background: rgb(98,184,250);
		font-family: “Comic Sans MS”;
		padding: 10px;
		font: 20px;
	}
</style>
</head>
<body>
	<div class="panel panel-primary form-horizontal">
		<div class="panel-heading" style="text-align: center;">
			<h3>
				เงินเดือน
			</h3>
		</div>
		<div class="panel-body">
	<table class="table table-striped table-bordered" width="80%" >
		<thead>

			<th >
				จำนวนวัน
			</th>
			<th>
				ค่าแรง
			</th>
			<th>
				OT(เงินพิเศษ)
			</th>
			<th>
				ยอดเบิกเงิน
			</th>
			<th>
				รวมเงินเดือน
			</th>
			<th>
				ยอดเงินคงเหลือ
			</th>
		</thead><?php 
				$datenow =date('Y-m-d'); //echo $datenow;
				$days =  date('d'); 
				$month = date('m');
				$year = date('Y');
				//echo $days."/".$month."/".$year;

				$endmonth = date("t",strtotime($datenow));
				$iduser = $objresult['use_id'];		
				$selectsummoney = 	"	select 
										(
											select count(job_date)as das 
											from jobsheet 
											where jobsheet.use_id = ' $iduser ' 
												and  job_date between  
												'$year-$month-1' 
												and '$year-$month-$endmonth'
										) as numdate,

										us.use_money as wage,

										(
											select sum(occ_num) 
											from occupier as oc,user 
											where user.use_id = oc.use_id 
												and oc.use_id = ' $iduser '
												and occ_date between  
												'$year-$month-1' 
												and '$year-$month-$endmonth'
										) as sumocc , 

										(
											select count(job_date)as das 
											from jobsheet 
											where jobsheet.use_id = ' $iduser ' 
												and  job_date between  
												'$year-$month-1' 
												and '$year-$month-$endmonth'
										)
										as dates ,

										(
											select sum(job_bonus)
											from jobsheet 
											where jobsheet.use_id = ' $iduser ' 
												and  job_date between  
												'$year-$month-1' 
												and '$year-$month-$endmonth'
										) as jobbonus

									from jobsheet,user as us 
									where jobsheet.use_id = us.use_id  
										and  jobsheet.use_id = ' $iduser '    
										group by jobsheet.use_id 
								";
								$qrmoneyuser = mysqli_query($condb,$selectsummoney);
								$objmoney = mysqli_fetch_array($qrmoneyuser,MYSQLI_ASSOC);
								if ($objmoney['sumocc'] =="") 
								{
									$objmoney['sumocc'] = 0;
								}
								$sumwage = $objmoney['wage'] * $objmoney['numdate'];
								$totalwages = $sumwage + $objmoney['jobbonus'];
								$salary = $totalwages - $objmoney['sumocc'];
						 ?>
			
		<tr align="center">
			<td>
				<?php echo $objmoney['numdate']; ?>
			</td>
			<td>
				<?php echo $objmoney['wage']; ?>
			</td>
			<td>
				<?php echo $objmoney['jobbonus']; ?>
			</td>
			<td>
				<?php echo $objmoney['sumocc']; ?>
			</td>
			<td>
				<?php echo $totalwages; ?>
			</td>
			<td>
				<?php echo $salary ?>
			</td>
		</tr>

	</table>
	</div>
	</div>
	<?php  mysqli_close($condb); ?>
</body>
</html>

				
