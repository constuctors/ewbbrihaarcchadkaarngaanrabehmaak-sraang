<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "personal" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ พนักงานกรุณาออกไปครับ!');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	//echo $objresult["use_fname"];	
		
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>ผู้ดูแลระบบ<?php echo $objresult['username']; ?></title>

		<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
		<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	</head>
		<style>
		.block-menu{
		border: solid 1px #D4EBFD ;
		background-color: rgb(15,146,245);
		margin: 0;
		border-radius: 5px;
		width: 100%;
		
	}
	
	.block-body{
		border: solid 1px #D4EBFD ;
		margin-left: 20px;
		margin-top: 10px;
		margin-right: 10px;
		border-radius: 5px;
		padding: 15px ;
	}
	.text-alite{
		margin: 5px;
		font-family: 'Designil Font', 'Helvetica', sans-serif;

		
	}
	.box1{
		border: solid 2px #D4EBFD; 
		padding: 30px;
		width: 100%;
		border-radius: 10px;
		background: rgb(98,184,250);
	}
	.from{
		padding: 10px 0px;
	}
	.text-alite li{
		list-style-type: none;
		padding:10px;
	}

	.m-list{
		padding-left: 0;
		margin-top: 0;
		margin-bottom: 0;
		 font-family: 'Designil Font', 'Helvetica', sans-serif;
	}
	.m-list li{
		
		list-style-type: none;
		padding: 10px;
	}
	.m-list li a{
		text-decoration: none;
		color: inherit;
	}
	.m-list li:hover{
		background: rgb(98,184,250);
		color: white;
	}
				
</style>


<body>
		
<nav class="navbar navbar-default" style="background-color: #3498DB;">
	<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ช่างจ๊อดรับเหมาก่อสร้าง</a>
		</div>

    <!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
			<ul class="nav navbar-nav navbar-right" >
				<li class="menu-item current-menu-item"><a href="indexP.php">หน้าแรก</a></li>
				<li class="menu-item"><a href=" "><?php echo $objresult['use_fname'];  echo "&nbsp;".$objresult['use_lname']; ?></a></li>
				<li class="menu-item"><a href="../logout.php">ออกจากระบบ</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>		<!-- Default snippet for navigation -->
<div class="container theme-showcase" role="main" >
	<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-2">
			<div class="list-group">
				<a href="indexP.php" class="list-group-item ">
					หน้าแรก
				</a>
				<a href="indexP.php?url=editdetailpersonal.php" class="list-group-item">
					แก้ไขข้อมูลส่วนตัว
				</a>
				<a href="indexP.php?url=occupier.php" class="list-group-item">
					เบิกงานล่วงหน้า
				</a>
				<a href="indexP.php?url=salary.php" class="list-group-item">
					คำนวนเงินเดือน
				</a>
				<a href="indexP.php?url=occshow_money.php" class="list-group-item">
					รายการเบิกเงินล่วงหน้า
				</a>
				<a href="indexP.php?url=showdate.php" class="list-group-item">
					เช็ควันทำงาน
				</a>
				<a href="../logout.php" class="list-group-item">
					ออกจากระบบ
				</a>
			</div>
		</div>
		<div class="col-xs-12 col-sm-9 col-md-10">
			<div class="row">
					<?php 
						if ($_GET['url'] == "") 
						{
							include_once("profile.php"); 
						}else{
							include_once( $_GET['url']); 
						}
					?>
			</div>			
		</div>	

		<div class="col-xs-12 col-sm-6 col-md-2"></div>
	</div>
</div>
	


</body>