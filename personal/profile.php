<?php 
	ob_start();
	session_start();
 ?>
 <?php 
	if($_SESSION['use_id'] == "")
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('Please Login!');window.location='../index.php';";
		echo "</script>";
		exit();
	}
		
	if ($_SESSION["status_name"] != "personal" )
	{
		echo "<script language=\"JavaScript\">";
		echo "alert('คูณไม่ใช่ พนักงานกรุณาออกไปครับ!');window.location='../index.php';";
		echo "</script>";
		exit();
	}


	require '../connect/connecDb.php';
	$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
	$result = mysqli_query($condb,$query);
	$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);

	$seluser = 	"	select *
				from user , office
				where use_id = ' ".$_SESSION['use_id']. " ' and user.off_id = office.off_id
			";
	$qruser = mysqli_query($condb,$seluser);
	$objuser = mysqli_fetch_array($qruser);
		
		
 ?>
<div class="panel panel-primary form-horizontal">
	<div class="panel-heading" style="text-align: center;">
		<h3>
			ข้อมูลส่วนตัว
		</h3>
	</div>
	<div class="panel-body">
		<div class="col-md-3" >
			<div class="col-md-12">
				<img src="../picture/<?php echo $objresult['use_image']; ?>"  style="width:100%; " class="img-rounded" >
			</div>
		</div>
		<div class="col-md-9">
			<div class="form-group">
				<div class="col-md-3">
					<label  for="exampleInputName2 ">ชื่อ - นามสกุล:</label>
				</div>
				<div class="col-md-9">
					<input type="text" readonly class="form-control col-xs-4" value="<?php echo $objresult['use_fname']."&nbsp;".$objresult['use_lname'];?>">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3">
					<label  for="exampleInputName2 ">อายุ :</label>
				</div>
				<div class="col-md-3">
					<input type="text" readonly class="form-control" value="<?php echo $objresult['use_age'];?>">
				</div>
				<div class="col-md-2">
					<label  for="exampleInputName2">ชื่อเล่น:</label>
				</div>
				<div class="col-md-4">
					<input type="text" readonly class="form-control" value="<?php echo $objresult['use_nname'];?>">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-3">
					<label  for="exampleInputName2 ">เบอร์โทรศัพท์:</label>
				</div>
				<div class="col-md-3">
					<input type="text" readonly class="form-control" value="<?php echo $objresult['use_phone'];?>">
				</div>
				<div class="col-md-2">
					<label  for="exampleInputName2 ">สัญชาติ:</label>
				</div>
				<div class="col-md-4">
					<input type="text" readonly class="form-control" value="<?php echo $objresult['use_nationality'];?>">
				</div>
				
			</div>
			<div class="form-group">
				<div class="col-md-3">
					<label  for="exampleInputName2 ">ตำแหน่งงาน :</label>
				</div>
				<div class="col-md-3">
					<input type="text" readonly class="form-control" value="<?php echo $objuser['off_name'];?>">
				</div>
				<div class="col-md-2">
					<label  for="exampleInputName2 ">ค่าแรง :</label>
				</div>
				<div class="col-md-4">
					<input type="text" readonly class="form-control" value="<?php echo $objuser['use_money']." บาท";?>">
				</div>
				
			</div>
			<div class="form-group" >
				<div class="col-md-3">
					<label  for="exampleInputName2" style="font-size: 16px;">ที่อยู่:</label>
				</div>
				<div class="col-md-9"><textarea class="form-control" readonly   name="use_address" id="use_address"><?php echo $objresult['use_address']; ?>
					</textarea>
				</div>
			</div>
		</div>

	</div>
</div>

<?php  mysqli_close($condb); ?>
