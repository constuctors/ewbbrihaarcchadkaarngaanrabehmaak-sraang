<?php 
	ob_start();
	session_start();


	if ($_SESSION['use_id'] != "")
	{
		if ($_SESSION["status_name"] != "user" )
		{
			echo "<script language=\"JavaScript\">";
			echo "alert('คูณไม่ใช่ ลูกค้ากรุณาออกไปครับ');window.location='index.php';";
			echo "</script>";
		}
		require 'connect/connecDb.php';
		$query = "select * from user where use_id = ' ".$_SESSION['use_id']. " ' ";
		$result = mysqli_query($condb,$query);
		$objresult = mysqli_fetch_array($result,MYSQLI_ASSOC);
	}		

 ?>
 <!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>แก้ไขข้อมูลส่่วนตัว</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="css/animate.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/bootstrap.min.js"></script>
    	<script>
			$(document).ready(function() 
			{
				$("#showprovince").change(function()
				{
					$.ajax({
						url: 'searchpro.php',
						type: "GET",
						data: { id : $(this).val() } ,
						success : function(result){
									$("#showarea").html(result);
									
							}
					})
				});
			});
		</script>
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>

	<body>
	<div id="site-content">
		<header class="site-header">
			<div class="top-header">
				<div class="container">
					<a href="tel:80049123441">Call Us: 086-478-1761</a>
					<nav class="member-navigation pull-right">
						<?php  if($_SESSION['use_id'] != "")
							{
						?>
							<a href="detailuser.php"><i class="fa fa-user"></i>
								<?php echo $objresult['use_fname'];echo "&nbsp";echo $objresult['use_lname'];?>
							</a>
							<a href="logout.php"><i class="fa fa-lock"></i> ออกจากระบบ</a>
						<?php 
							}else{ 
						?>
						<a href="fromLogin.php"><i class="fa fa-user"></i> Register</a>	
						<a href="fromLogin.php"><i class="fa fa-lock"></i> Login</a>
						<?php 	} ?>
					</nav> <!-- .member-navigation -->
				</div> <!-- .container -->
			</div> <!-- .top-header -->
			<div class="bottom-header">
				<div class="container">
					<a href="index.php" class="branding pull-left">
						<img src="images/logo-icon.png" alt="Site title" class="logo-icon">
						<h1 class="site-title">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></h1> 
						<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
					</a> <!-- #branding -->
						
					<nav class="main-navigation pull-right">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="profilenews.php">ผลงาน</a></li>
							<li class="menu-item"><a href="showjob.php">ตรวจสอบราคางาน</a></li>
							<li class="menu-item"><a href="contact.php">ติดต่องาน</a></li>
						</ul>
					</nav> <!-- .main-navigation -->
				</div> <!-- .container -->
			</div> <!-- .bottom-header -->
			</header> <!-- .site-header -->

<main class="content">
	<div class="container">
		<div class="rows">
			<div class="col-md-12" style="margin: 10px;"> 
				<a href="detailuser.php">
					<button type="button" class="btn btn-danger">
						ยกเลิกการแก้ไข
					</button>
				</a>
			</div>
					
			<?php 
				$id = $_GET['use_id'];
				$s_id =  	"	select * 
							from user
							where  use_id = '$id'
						";
				$qr_id = mysqli_query($condb,$s_id);
				$objqr_id = mysqli_fetch_array($qr_id,MYSQLI_ASSOC);
				
			 ?>
			
			
			<div class="col-md-8 col-md-offset-2"  style="background-color: #F5F5F5;  border-radius: 5px; padding: 10px;">

				<h2 style="text-align: center;">แก้ไขข้อมูลส่วนตัว</h2>

				<form action="addmin/editP_update.php"  class="form-horizontal" method = "post" enctype="multipart/form-data">
					<div class="contact-form ">
						<input type="hidden" name="use_id" id="use_id" style="width: 300px" readonly="true" value="<?php echo $objqr_id['use_id']; ?>">
						<div class="form-group ">
							<center><img src="picture/<?php echo $objqr_id['use_image']; ?>"  width = "100" hieght = "100"><br></center>
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">ชื่อ :</label>
						<div class="col-sm-4">
							<input type="text" name="use_fname" class="form-control" value="<?php echo $objqr_id['use_fname']; ?>">
						</div>

						<label class="col-sm-2 control-label">นามสกุล :</label>
						<div class="col-sm-4">
							<input type="text" name="use_lname" class="form-control" value="<?php echo $objqr_id['use_lname']; ?>">
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">ชื่อเล่่น :</label>
						<div class="col-sm-4">
							<input type="text" name="use_nname" class="form-control" value="<?php echo $objqr_id['use_nname']; ?>">
						</div>

						<label class="col-sm-2 control-label">อายุ :</label>
						<div class="col-sm-4">
							<input type="text" name="use_age" class="form-control" value="<?php echo $objqr_id['use_age']; ?>">
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">วัน/เดือน/ปีเกิด (01/02/1990) :</label>
						<div class="col-sm-4">
							<input type="text" name="use_birthday" class="form-control" value="<?php echo $objqr_id['use_birthday']; ?>">
						</div>
						<label class="col-sm-2 control-label">เบอร์โทรศํพท์ :</label>
						<div class="col-sm-4">
							<input type="text" name="use_phone" class="form-control" value="<?php echo $objqr_id['use_phone']; ?>">
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">ที่อยู่ :</label><div class="col-sm-10"><textarea name="use_address" class="form-control" rows="3px" ><?php echo $objqr_id['use_address']; ?>
								</textarea>
						</div>
					</div>
					<div class="form-group ">
							<label class="col-sm-2 control-label">จังหวัด :</label>
							<div class="col-sm-4">
								<select id="showprovince" name="PROVINCE_ID" class="form-control">

								<?php 
									
									if ($objqr_id['PROVINCE_ID'] != 0) 
									{
								 ?>
							 		</option>
										<?php 
											$slpr = 	" 	select *
															from province
															order by PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."' desc
														";
											$qr = mysqli_query($condb,$slpr);
											while ($arr = mysqli_fetch_array($qr)) 
											{
										 ?>
									
									<option value="<?php echo $arr['PROVINCE_ID']; ?>">	
										<?php echo $arr['PROVINCE_NAME']; ?>
									</option>
								<?php
											}
									}else
								{
									 ?>
									<option value="">
									=====กรุณาเลือกจังหวัด=====
									</option>
										<?php 
											$slpr = 	" 	select *
															from province
														";
											$qr = mysqli_query($condb,$slpr);
											while ($arr = mysqli_fetch_array($qr)) 
											{
										 ?>
									
									<option value="<?php echo $arr['PROVINCE_ID']; ?>">	
										<?php echo $arr['PROVINCE_NAME']; ?>
									</option>
									<?php }
								}

									 ?>
									
								</select>
							</div>
							
							<label class="col-sm-2 control-label">อำเภอ / แขวง :</label>
							<div class="col-sm-4" id="showarea">
								<select name="AMPHUR_ID" class="form-control" id="ampid">
									<?php 
										if ($objqr_id['AMPHUR_ID'] != 0) 
										{
									 ?>
								 		</option>
											<?php 
												$slam = 	" 	select *
																from amphur
																where PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."'
																order by AMPHUR_ID = ' "  .$objqr_id['AMPHUR_ID']. " ' desc
															";
												$qr = mysqli_query($condb,$slam);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['AMPHUR_ID']; ?>">	
											<?php echo $arr['AMPHUR_NAME']; ?>
										</option>
										<?php
												}
									}else
									{
										 ?>
										<option value="">
										=====กรุณาเลือกอำเภอ=====
										</option>
											<?php 
												$slam = 	" 	select *
																from amphur
															";
												$qr = mysqli_query($condb,$slam);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['AMPHUR_ID']; ?>">	
											<?php echo $arr['AMPHUR_NAME']; ?>
										</option>
										<?php }
									}

										 ?>
								</select>
							</div>
						</div>
						<div class="form-group ">
							<label class="col-sm-2 control-label">ตำบล / เขต :</label>
							<div class="col-sm-4" id="showsubarea">	
								<select name="DISTRICT_ID" class="form-control" id="ampid">
									<?php 
										if ($objqr_id['DISTRICT_ID'] != 0) 
										{
									 ?>
								 		</option>
											<?php 
												$slds = 	" 	select *
																from district
																where 	PROVINCE_ID = '".$objqr_id['PROVINCE_ID']."'
																		and AMPHUR_ID = ' ".$objqr_id['AMPHUR_ID']." '
																order by DISTRICT_ID = ' "  .$objqr_id['DISTRICT_ID']. " ' desc
															";
												$qr = mysqli_query($condb,$slds) or die(mysqli_error($condb));
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['DISTRICT_ID']; ?>">	
											<?php echo $arr['DISTRICT_NAME']; ?>
										</option>
										<?php
												}
									}else
									{
										 ?>
										<option value="">
										=====กรุณาเลือกตำบล=====
										</option>
											<?php 
												$slds = 	" 	select *
																from district
															";
												$qr = mysqli_query($condb,$slds);
												while ($arr = mysqli_fetch_array($qr)) 
												{
											 ?>
										
										<option value="<?php echo $arr['DISTRICT_ID']; ?>">	
											<?php echo $arr['DISTRICT_NAME']; ?>
										</option>
										<?php }
									}

										 ?>
								</select>
							</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">รหัสไปรษณีย์ :</label>
						<div class="col-sm-4">
							<input type="text" name="use_postalcode" class="form-control" value="<?php echo $objqr_id['use_postalcode']; ?>">
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 control-label">สัญชาติ :</label>
						<div class="col-sm-2">
							<input type="text" name="use_nationality" class="form-control" value="<?php echo $objqr_id['use_nationality']; ?>">
						</div>
						<label class="col-sm-2 control-label">เชื้อชาติ :</label>
						<div class="col-sm-2">
							<input type="text" name="use_race" class="form-control" value="<?php echo $objqr_id['use_race']; ?>">
						</div>
						<label class="col-sm-2 control-label">ศาสนา :</label>
						<div class="col-sm-2">
							<input type="text" name="use_religion" class="form-control" value="<?php echo $objqr_id['use_religion']; ?>">
						</div>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-3 control-label">เพศ :</label>
						<label class="radio-inline col-sm-1 col-xs-3">
							<input type="radio" name="use_sex" value="1" style="width: 20px" <?php if ($objqr_id['use_sex'] == 1) {
									echo "checked";
								}
								?>>
							ชาย
						</label>
						<label class="radio-inline col-sm-1 col-xs-3">
							<input type="radio" name="use_sex" value="2" style="width: 20px" <?php 
								if ($objqr_id['use_sex'] == 2)
								 {
									echo "checked";
								}?>>
							หญิง
						</label>
					</div>
					<div class="form-group ">
						<label class="col-sm-2 col-xs-12 control-label">
							การศึกษา :
						</label>	
						<div class="col-sm-4 col-xs-12">
							<select name="edu_id" class="form-control">
								<?php 
									$sel = "select * from education ORDER BY edu_id = ' ".$objqr_id['edu_id']." ' DESC";
									$res = mysqli_query($condb,$sel);
										while ( $row = mysqli_fetch_array($res)) 
										{
								 ?>
								<option value="<?php echo $row["edu_id"]; ?>">
								<?php 
										echo $row["edu_name"]; 
										}
								?>
								</option>
							</select>
						</div>
					</div>
				<div class="col-md-12" style="margin: 10px; text-align: center;">
					<button type="submit" class="btn btn-success">
						แก้ไขข้อมูล
					</button>
				</div>
				</form>
			</div>
		</div>
	</div>
</main> <!-- .content -->
			<footer class="site-footer wow fadeInUp">
				<div class="container">

					<div class="row">
						<div class="col-md-6">
							
							<div class=" branding">
								<img src="images/logo-footer.png" alt="Site title" class="logo-icon">
								<h1 class="site-title"><a href="#">บริษัท <span>ช่างจ๊อดรับเหมาก่อสร้าง</span></a></h1> 
								<h2 class="site-description">เราสร้างได้ถ้าคุณต้องการ</h2>
							</div> <!-- .branding -->

							<p class="copy">Copyright 2014 บริษัทช่างจ๊อดรับเหมาก่อสร้าง. designed by Themezy. All rights reserved</p>
						</div>
						
						<div class="col-md-6 align-right">
						
							<nav class="footer-navigation">
								<a href="profilenews.php">ผลงาน</a>
								<a href="showjob.php">ตรวจสอบราคางาน</a>
								<a href="contact.php">ติดต่องาน</a>
							</nav> <!-- .footer-navigation -->
							<!--
							<div class="social-links">
								<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
								<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
								<a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
								<a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
							</div>  .social-links -->
						
						</div>
					</div>
				</div>
			</footer> <!-- .site-footer -->
			

		</div> <!-- #site-content -->
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>
</html>